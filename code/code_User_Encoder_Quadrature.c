//
// Created by 57Kindness on 2023/3/1.
//

#include "code_User_Encoder_Quadrature.h"

int16 encoder_data_1 = 0;
int16 left_flywheel_encoder_data = 0;                                 // 左侧飞轮编码器值
int16 right_flywheel_encoder_data = 0;                                // 右侧飞轮编码器值

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     电机编码器 计数值刷新
// 参数说明
// 返回参数
// 使用示例     Encoder_Counter();
// 备注信息     此函数放在对应中断中使用，建议读取时间为5ms
// 作者信息     Created by 57Kindness on 2023/3/1.
//-------------------------------------------------------------------------------------------------------------------
sint16 Encoder_GetCounter(void) {
    encoder_data_1 = encoder_get_count(ENCODER_1);                              // 获取编码器计数
    encoder_clear_count(ENCODER_1);                                             // 清空编码器计数
    return encoder_data_1;
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     左飞轮编码器读数 计数值刷新
// 参数说明
// 返回参数
// 使用示例     Left_FlyWheel_Encoder_GetCounter();
// 备注信息     此函数放在对应中断中使用，建议读取时间为5ms
// 作者信息     Created by 57Kindness on 2023/3/28.
//-------------------------------------------------------------------------------------------------------------------
sint16 Left_FlyWheel_Encoder_GetCounter(void) {
    left_flywheel_encoder_data = encoder_get_count(LEFT_FLYWHEEL_ENCODER_INDEX);   // 获取左侧编码器值
    encoder_clear_count(LEFT_FLYWHEEL_ENCODER_INDEX);                               // 清除编码器计数 方便下次采集
    return left_flywheel_encoder_data;
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     右飞轮编码器读数 计数值刷新
// 参数说明
// 返回参数
// 使用示例     Right_FlyWheel_Encoder_GetCounter();
// 备注信息     此函数放在对应中断中使用，建议读取时间为5ms
// 作者信息     Created by 57Kindness on 2023/3/28.
//-------------------------------------------------------------------------------------------------------------------
sint16 Right_FlyWheel_Encoder_GetCounter(void) {
    right_flywheel_encoder_data = encoder_get_count(RIGHT_FLYWHEEL_ENCODER_INDEX);   // 获取右侧编码器值
    encoder_clear_count(RIGHT_FLYWHEEL_ENCODER_INDEX);                               // 清除编码器计数 方便下次采集
    return right_flywheel_encoder_data;
}
