/*菜单修改方法：
 * 1.修改头文件宏定义row个数
 * 2.修改info_found
 * 3.修改二级菜单指针函数数组
 * 4.增加子菜单
 * 5.增加子菜单函数头文件
 * 6.子菜单内修改flash保存地址
 * 7.note:字菜单中可修改数据数目最多为3个，因为不想浪费太多flash_memory数组，可更改头文件MAX_MENU2_NUM
 */

#include "code_User_menu.h"

//开关状态变量
bool Key1_result;
bool Key2_result;
bool Key3_result;
bool Key4_result;

//开关状态变量
bool key1_status = 1;
bool key2_status = 1;
bool key3_status = 1;
bool key4_status = 1;

bool Button1_status = 1;
bool Button2_status = 1;
bool Button3_status = 1;

//上一次开关状态变量
bool key1_last_status;
bool key2_last_status;
bool key3_last_status;
bool key4_last_status;

bool Button1_last_status;
bool Button2_last_status;
bool Button3_last_status;

bool clear_flag = 0;

//一级菜单
bool menu_level = 0;
unsigned char menu_index = 0;
char menustr[20];


//二级菜单
unsigned char menu2_index = 0;
unsigned char menu2_mode = 0;//
unsigned char menu2_limit = 0;//子菜单内选项个数
bool menu2_level = 0;

//  修改倍数相关
unsigned char magindex = 0;
float mag[] = {0.01, 0.1, 1, 10, 100};

//数值修改相关
int *intvalue[MAX_MENU2_NUM];
float *floatvalue[MAX_MENU2_NUM];

//flash保存
unsigned char flash_index = 0;


// 一级菜单指针函数
void (*menu_pfc[])(unsigned char) = {menu_select, menu2_select};

char (*amenu2_init_pfc[])(char) = {menu2_speed,
                                   menu2_palst_L_pid,
                                   menu2_palst_R_pid,
                                   menu2_angle_L_pid,
                                   menu2_angle_R_pid,
                                   menu2_speed_L_pid,
                                   menu2_speed_R_pid,
                                   menu2_balance_pid,
                                   menu2_vec_pid,
                                   menu2_Angle_zero,
                                   menu2_Turn};


void button_init(void) {
    //按键开关
    gpio_init(P22_0, GPI, 1, GPI_FLOATING_IN);
    gpio_init(P22_1, GPI, 1, GPI_FLOATING_IN);
    gpio_init(P22_2, GPI, 1, GPI_FLOATING_IN);
    gpio_init(P22_3, GPI, 1, GPI_FLOATING_IN);

    //拨码开关
    gpio_init(P33_11, GPI, 1, GPI_FLOATING_IN);
    gpio_init(P33_9, GPI, 1, GPI_FLOATING_IN);
    gpio_init(P33_13, GPI, 1, GPI_FLOATING_IN);
    gpio_init(P33_12, GPI, 1, GPI_FLOATING_IN);
    gpio_init(P23_1, GPI, 1, GPI_FLOATING_IN);
    gpio_init(P32_4, GPI, 1, GPI_FLOATING_IN);
}


void Key_entry(void) {
    key1_last_status = key1_status;
    key2_last_status = key2_status;
    key3_last_status = key3_status;
    key4_last_status = key4_status;

    Button1_last_status = Button1_status;
    Button2_last_status = Button2_status;
    Button3_last_status = Button3_status;

    //读取当前按键状态
    key1_status = gpio_get_level(P22_0);
    key2_status = gpio_get_level(P22_1);
    key3_status = gpio_get_level(P22_2);
    key4_status = gpio_get_level(P22_3);

    Button1_status = gpio_get_level(P33_11);
    Button2_status = gpio_get_level(P33_9 );
    Button3_status = gpio_get_level(P33_12);

    if (key1_status && !key1_last_status) Key1_result = 1;
    if (key2_status && !key2_last_status) Key2_result = 1;
    if (key3_status && !key3_last_status) Key3_result = 1;
    if (key4_status && !key4_last_status) Key4_result = 1;

    if (gpio_get_level(P33_11) && gpio_get_level(P33_9 ) && gpio_get_level(P33_12)){
        if (Button1_status && !Button1_last_status ) ips200_clear();
        if (Button2_status && !Button2_last_status ) ips200_clear();
        //if (Button3_status && !Button3_last_status ) ips200_clear();
    }
}

void menu_entry(void) {
    // 按下key1
    if (Key1_result) {
        Key1_result = 0;
        menu_pfc[menu_level](1);
    }
    //按下key2
    if (Key2_result) {
        Key2_result = 0;
        menu_pfc[menu_level](2);
    }
    //按下key3
    if (Key3_result) {
        Key3_result = 0;
        menu_pfc[menu_level](3);
    }
    //按下key4
    if (Key4_result) {
        Key4_result = 0;
        menu_pfc[menu_level](4);
    }

}


void menu_select(unsigned char event) {
    if (!menu_level) {
        switch (event) {
            case 1://选项上移
                if (menu_index > 0) menu_index--;
                else menu_index = ROWS - 1;
                break;
            case 2://选项下移
                if (menu_index < ROWS - 1) menu_index++;
                else menu_index = 0;
                break;
            case 3://进入二级菜单
                menu2_init();
                menu_level = 1;
                return;
        }

    }


}


void menu2_init() {
    //  变量定义及初始化
    magindex = 1;
    menu2_level = 0;
    menu2_index = 0;
    menu2_limit = 0;
    clear_flag = 1;
}


//数值修改模块
static void modify(unsigned char event) {
    switch (menu2_mode) {
        case PARASET_F:
            if (event) *floatvalue[menu2_index] += mag[magindex];//数值增加
            else *floatvalue[menu2_index] -= mag[magindex];//数值减少
            break;
        case PARASET_S:
            if (event) *intvalue[menu2_index] += mag[magindex];
            else *intvalue[menu2_index] -= mag[magindex];
            break;
    }
}


void menu2_select(unsigned char event) {
    if (!menu2_level) {
        switch (event) {
            case 1:
                if (menu2_index > 0) menu2_index--;
                break;
            case 2:
                if (menu2_index < menu2_limit) menu2_index++;
                break;
            case 3:
                menu2_level = 1;
                break;
        }

    } else {
        switch (event) {
            case 1:
                modify(1);
                break; //数值增
            case 2:
                modify(0);
                break; //数值减
            case 3: //确定键
                clear_flag = 1;
                menu2_level = 0;
                flash_memory_write();
                return;
            case 4: //增值倍数修改
                magindex++;
                if (magindex > 4) magindex = 0;
                break;
        }


    }


}


void menu_display(void) {
    register char i;
    if (!menu_level) {
        for (i = 0; i < ROWS; i++) {
            if (i == menu_index)ips200_bgcolor = RGB565_BLUE;
            else ips200_bgcolor = RGB565_BLACK;
            info_found(i);
            ips200_menu_show_string(0, i, menustr);
        }
    }


}


void menu2_display(void) {

    unsigned char i;
    ips200_bgcolor = RGB565_BLACK;
    ips200_menu_show_float(170, 10, mag[magindex], 3, 3); //显示倍数
    for (i = 0; i <= menu2_limit; i++) {
        amenu2_init_pfc[menu_index](i);
        if (!menu2_level) {
            if (menu2_index == i)ips200_bgcolor = RGB565_BLUE;
            else ips200_bgcolor = RGB565_BLACK;
            ips200_menu_show_string(0, i, menustr);
        } else {
            if (menu2_mode == PARASET_S)
                ips200_menu_show_int(150, menu2_index, *intvalue[menu2_index], 4);
            else
                ips200_menu_show_float(150, menu2_index, *floatvalue[menu2_index], 4, 3);
        }
    }
    //返回一级菜单(放在二级菜单选择函数中有bug)
    if (menu2_index == menu2_limit) {
        menu_level = 0;
        ips200_bgcolor = RGB565_BLACK;
        clear_flag = 1;

    }


}

/******************一级菜单选项显示**********************/
//一级菜单选项显示
void info_found(unsigned char index) {
    memset(menustr, 0, sizeof menustr);          //清空数组
    switch (index) {
        case 0:
            strcpy(menustr, "speed");
            break;
        case 1:
            strcpy(menustr, "palst_L_pid");
            break;
        case 2:
            strcpy(menustr, "palst_R_pid");
            break;
        case 3:
            strcpy(menustr, "angle_L_pid");
            break;
        case 4:
            strcpy(menustr, "angle_R_pid");
            break;
        case 5:
            strcpy(menustr, "speed_L_pid");
            break;
        case 6:
            strcpy(menustr, "speed_R_pid");
            break;
        case 7:
            strcpy(menustr, "balance_pid");
            break;
        case 8:
            strcpy(menustr, "vec_pid");
            break;
        case 9:
            strcpy(menustr, "Angle_zero");
            break;
        case 10:
            strcpy(menustr, "Turn.pid");
            break;
    }
}







char menu2_speed(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 1;
    //  修改数值的地址
    floatvalue[0] = &Set_Speed;
    //flash地址
    flash_index = 0;
    switch (index) {
        case 0:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "set_speed");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;

}


char menu2_palst_L_pid(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Palst_L.Kp;
    floatvalue[1] = &Palst_L.Ki;
    floatvalue[2] = &Palst_L.Kd;
    //flash地址
    flash_index = 1;
    switch (index) {
        case 0:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Palst_L.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Palst_L.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Palst_L.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}

char menu2_palst_R_pid(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Palst_R.Kp;
    floatvalue[1] = &Palst_R.Ki;
    floatvalue[2] = &Palst_R.Kd;
    //flash地址
    flash_index = 2;
    switch (index) {
        case 0:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Palst_R.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Palst_R.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Palst_R.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}




char menu2_angle_L_pid(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Angle_L.Kp;
    floatvalue[1] = &Angle_L.Ki;
    floatvalue[2] = &Angle_L.Kd;
    //flash地址
    flash_index = 3;
    switch (index) {

        case 0:
            //菜单名称初始化
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Angle_L.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Angle_L.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Angle_L.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}

char menu2_angle_R_pid(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Angle_R.Kp;
    floatvalue[1] = &Angle_R.Ki;
    floatvalue[2] = &Angle_R.Kd;
    //flash地址
    flash_index = 4;
    switch (index) {

        case 0:
            //菜单名称初始化
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Angle_R.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Angle_R.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Angle_R.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}


char menu2_speed_L_pid(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Speed_L.Kp;
    floatvalue[1] = &Speed_L.Ki;
    floatvalue[2] = &Speed_L.Kd;
    //flash地址
    flash_index = 5;
    switch (index) {

        case 0:
            //菜单名称初始化
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Speed_L.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Speed_L.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Speed_L.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}

char menu2_speed_R_pid(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Speed_R.Kp;
    floatvalue[1] = &Speed_R.Ki;
    floatvalue[2] = &Speed_R.Kd;
    //flash地址
    flash_index = 6;
    switch (index) {

        case 0:
            //菜单名称初始化
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Speed_R.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Speed_R.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Speed_R.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}


char menu2_balance_pid(char index) {

    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Balance.Kp;
    floatvalue[1] = &Balance.Ki;
    floatvalue[2] = &Balance.Kd;
    //flash地址
    flash_index = 7;
    switch (index) {

        case 0:
            //菜单名称初始化
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Balance.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Balance.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Balance.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}


char menu2_vec_pid(char index) {

    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Vec.Kp;
    floatvalue[1] = &Vec.Ki;
    floatvalue[2] = &Vec.Kd;
    //flash地址
    flash_index = 8;
    switch (index) {

        case 0:
            //菜单名称初始化
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Vec.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Vec.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Vec.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}


char menu2_Angle_zero(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 2;
    //  修改数值的地址
    floatvalue[0] = &Angle_Fly;
    floatvalue[1] = &Angle_Mot;
    //flash地址
    flash_index = 9;
    switch (index) {

        case 0:
            //菜单名称初始化
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Angle_zero_Fly");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Angle_zero_Mot");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;

    }
    return 0;
}


char menu2_Turn(char index) {
    //菜单属性
    menu2_mode = PARASET_F;
    menu2_limit = 3;
    //  修改数值的地址
    floatvalue[0] = &Turn.Kp;
    floatvalue[1] = &Turn.Ki;
    floatvalue[2] = &Turn.Kd;
    //flash地址
    flash_index = 10;
    switch (index) {
        case 0:
            //菜单名称初始化
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Turn.Kp");
            return 0;
        case 1:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Turn.Ki");
            return 0;
        case 2:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "Turn.Kd");
            return 0;
        case 3:
            memset(menustr, 0, sizeof menustr);          //清空数组
            strcpy(menustr, "exit");
            return 0;
        case FLASH_INIT:
            menu2_mode = PARASET_F;
            return menu2_limit;
    }
    return 0;
}

void display_entry(void) {
    if (clear_flag) {
        clear_flag = 0;
        ips200_clear();
    }
    if (!menu_level) menu_display();

    else menu2_display();
}



