#include "code_User_para_flash.h"

uint32 flash_memory[data_num];

/*--------------------------------------------------------------*/
/*                           变量定义                           */
/*==============================================================*/

/*--------------------------------------------------------------*/
/*                           函数定义                           */
/*==============================================================*/
/*--------------------------------------------------------------*/
/*                           flash存储                                                                              */
/*==============================================================*/
/*   整型转浮点型存储   */
/*======================*/
float ex_int2float(long int value) {
//  定义联合体
    union {
        float float_value;
        unsigned long int_value;
    } c;
//  存储数据转换
    c.int_value = value;
    return c.float_value;
}
/*----------------------*/
/*   浮点型转整型存储   */
/*======================*/
long int ex_float2int(float value) {
//  定义联合体
    union {
        float float_value;
        unsigned long int_value;
    } c;
//  存储数据转换
    c.float_value = value;
    return c.int_value;
}


//flash数据读取
void paraflash_read(void) {
    register short i, j;
    unsigned char num;
    for (i = 0; i < data_num; i++) flash_memory[i] = flash_read(0, i, uint32);
    for (i = 0; i < ROWS; i++) {
        num = amenu2_init_pfc[i](FLASH_INIT);
        switch (menu2_mode) {
            case PARASET_F:
                for (j = 0; j < num; j++) *floatvalue[j] = ex_int2float(flash_memory[flash_index * MAX_MENU2_NUM + j]);
                break;
            case PARASET_S:
                for (j = 0; j < num; j++) *intvalue[j] = flash_memory[flash_index * MAX_MENU2_NUM + j];
                break;
        }
    }
}


//flash参数写入
void flash_memory_write(void) {
    register short i;
    //数据写入

    switch (menu2_mode) {
        case PARASET_F:
            for (i = 0; i < menu2_limit; i++)
                flash_memory[flash_index * MAX_MENU2_NUM + i] = ex_float2int(*floatvalue[i]);
            break;
        case PARASET_S:
            for (i = 0; i < menu2_limit; i++) flash_memory[flash_index * MAX_MENU2_NUM + i] = *intvalue[i];
            break;
    }
    eeprom_erase_sector(0);//擦除扇区
    for (i = 0; i < data_num; i++)
        eeprom_page_program(0, i, &flash_memory[i]);

}



