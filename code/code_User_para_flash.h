
#ifndef _para_flash_H_
#define _para_flash_H_


#include "zf_common_headfile.h"
#include "code_User_headfile.h"
#include <stdint.h>


float ex_int2float(long int value);

long int ex_float2int(float value);

void paraflash_read(void);

void flash_memory_write(void);


#endif //_para_flash_H_
