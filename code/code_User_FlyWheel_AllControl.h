//
// Created by 57Kindness on 2023/2/27.
//

#ifndef CODE_USER_FLYWHEEL_ALLCONTROL_H
#define CODE_USER_FLYWHEEL_ALLCONTROL_H

#include "zf_common_headfile.h"
#include "code_User_headfile.h"

void FlyWheel_All_Run(int16 Duty_L, int16 Duty_R);


#endif //SEEKFREE_TC264_OPENSOURCE_LIBRARY_CODE_USER_FLYWHEEL_ALLCONTROL_H
