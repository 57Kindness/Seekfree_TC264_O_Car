#ifndef code_User__menu_h
#define code_User__menu_h

#include "zf_common_headfile.h"
#include "code_User_headfile.h"
#include <Platform_Types.h>

#define ROWS 11   //一级菜单选项数量
#define data_num 40

#define PARASET_F 0   //显示浮点型
#define PARASET_S 1   //显示短整型

#define FLASH_INIT 10

#define MAX_MENU2_NUM 3 //二级菜单子函数总可修改数据最大数目


extern bool clear_flag;
extern bool menu_level;
extern unsigned char menu_index;
extern unsigned char menu2_mode;
extern unsigned char menu2_limit;

extern int *intvalue[MAX_MENU2_NUM];
extern float *floatvalue[MAX_MENU2_NUM];

extern unsigned char flash_index;

extern char (*amenu2_init_pfc[])(char);

void button_init(void);

void Key_entry(void);

void menu_entry(void);

void display_entry(void);

void menu_select(unsigned char event);

void menu2_select(unsigned char event);

void menu2_init(void);

void menu_display(void);

void menu2_display(void);

void info_found(unsigned char index);

char menu2_speed(char index);

char menu2_palst_L_pid(char index);

char menu2_palst_R_pid(char index);

char menu2_angle_L_pid(char index);

char menu2_angle_R_pid(char index);

char menu2_speed_L_pid(char index);

char menu2_speed_R_pid(char index);

char menu2_balance_pid(char index);

char menu2_vec_pid(char index);

char menu2_Angle_zero(char index);

char menu2_Turn(char index);

#endif
