//
// Created by 57Kindness on 2023/2/27.
//

#ifndef CODE_USER_CTRL_H_
#define CODE_USER_CTRL_H_

#include "zf_common_headfile.h"
#include "code_User_headfile.h"


extern float Accel_x, Accel_y, Accel_z, Gyro_x, Gyro_y, Gyro_z;

extern float Pitch1, Roll1;
extern float Set_Speed;
extern float Angle_Fly,Angle_Mot;
extern unsigned char Start_Flag;
extern int16 Turn_Difference;
extern float Turn_increment ,Turn_Zero;
extern float Speed_L_Out,Angel_L_Out,Palst_L_Out;
extern float Speed_R_Out,Angel_R_Out,Palst_R_Out;


void PID_Init(void);

void Balance_All(void);

void Palst_loop_All(void);

void Angle_loop_All(void);

void Speed_loop_All(void);

//void Speed_loop_Count();

void Vec_Ctrl(void);

void Balance_Ctrl(void);

void Turn_loop(float Offset);

float smoothTransition(float target, float target_last, float increment);


#endif
