//
// Created by 57Kindness on 2023/2/27.
//

#ifndef CODE_USER_IMU_H
#define CODE_USER_IMU_H

#include "zf_common_headfile.h"
#include "code_User_headfile.h"


void Kalman_Filter_Euler(void);

void Kalman_Filter_X(float Accel, float Gyro);

void Kalman_Filter_Y(float Accel, float Gyro);

void Kalman_Filter_Z(float Accel, float Gyro);

float angle_diff(float a1, float a2);

extern float Pitch1, Roll1, Yaw1;

#endif








