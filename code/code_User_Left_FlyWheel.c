//
// Created by 57Kindness on 2023/2/27.
//

#include "code_User_Left_FlyWheel.h"

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     Left_FlyWheel 初始化
// 参数说明
// 返回参数
// 使用示例     Left_FlyWheel_Int ();
// 备注信息
// 作者信息     Created by 57Kindness on 2023/2/27.
//-------------------------------------------------------------------------------------------------------------------
void Left_FlyWheel_Int(void) {
    pwm_init(LEFT_FLYWHEEL_PWM, 12500,
             LEFT_FLYWHEEL_DUTY_LEVEL == 0 ? PWM_DUTY_MAX : 0);                  // 初始化左侧飞轮PWM信号
    gpio_init(LEFT_FLYWHEEL_DIR, GPO, LEFT_FLYWHEEL_CLOCKWISE,
              GPO_PUSH_PULL);                              // 初始化左侧飞轮DIR信号
    gpio_init(LEFT_FLYWHEEL_BRAKE, GPO, 1,
              GPO_PUSH_PULL);                                                  // 初始化左侧飞轮刹车信号
    encoder_quad_init(LEFT_FLYWHEEL_ENCODER_INDEX, LEFT_FLYWHEEL_ENCODER_CH1,
                      LEFT_FLYWHEEL_ENCODER_CH2);   // 初始化左侧飞轮编码器接口
}


//-------------------------------------------------------------------------------------------------------------------
// 函数简介     Left_FlyWheel 开转
// 参数说明     duty_out :输出占空比
// 返回参数     无
// 使用示例     Left_FlyWheel_Run (5000);
// 备注信息
// 作者信息     Created by 57Kindness on 2023/2/27.
//-------------------------------------------------------------------------------------------------------------------
void Left_FlyWheel_Run(int16 duty_out) {

    gpio_set_level(LEFT_FLYWHEEL_BRAKE, 1);                                     // 刹车信号取消

    if (duty_out > 0)                                                            // 根据占空比切换方向引脚
        gpio_set_level(LEFT_FLYWHEEL_DIR, LEFT_FLYWHEEL_CLOCKWISE);
    else
        gpio_set_level(LEFT_FLYWHEEL_DIR, !LEFT_FLYWHEEL_CLOCKWISE);

    pwm_set_duty(LEFT_FLYWHEEL_PWM, 10000 - func_abs(duty_out));  // 输出占空比 占空比必须为正值 因此此处使用绝对值

}
