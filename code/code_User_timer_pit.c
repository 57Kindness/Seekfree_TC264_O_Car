//
// Created by 57Kindness on 2023/6/15.
//

#include "code_User_timer_pit.h"

int count_Ring_judge;
int count_Ring_judge2;
//int count_Ring_judge3=-1;
int count_current_protect;
unsigned char current_count;
int Jump_Delay = -1;
int Jump_Delay_flag = -1;
int P_Delay = -1;
//int Three_cross_Delay=-1;
int Start_line_time = -1;
int type_delay = -1;
extern int length_val;//tof测距
extern float Jump_pitch;//
extern int Jump_flag; //坡道判断
//extern int start_flag;//发车
extern int run_flag; //运行
//extern unsigned char protect_flag; //保护
//extern int out_garage;
//extern int garage_yaw_init;
int motor_r_duty = 0;//
//extern float pitch;
//回调函数
void timer1_pit_entry(void *parameter) {

    static uint32 time;
    time++;


    if (time > 50000) time = 0;
    //陀螺仪解算
    // ICM_getEulerianAngles();//转成欧拉角

//    if(Three_cross_Delay>=0)
//    {
//        Three_cross_Delay--;
//    }

    if (type_delay >= 0) {
        type_delay--;
    }

//    //驱动电流保护
//    if(time%10==0)
//        Current_Protect();
//
//    //P环延时
//    if(Right_Ring_flag==2||Left_Ring_flag==2)
//    {
//        P_Delay=1000;
//    }
//    if(P_Delay>=0)
//        P_Delay--;
//起跑线延时
    if (Start_line_time <= 5000) {
        Start_line_time++;
    }
    //坡道判断
    if (type_delay <= 0 &&
        length_val <= 113 &&
        length_val != 0 &&
        !Jump_flag &&
        run_flag &&
        //count_Ring_judge3<0&&
        Jump_Delay < 0 &&
        Straight_Counter > 80) {
        Three_cross_count = Three_cross_count % 2 == 0 ? Three_cross_count : (Three_cross_count + 1) % 4;
        Jump_flag = 1;
        Jump_Delay_flag = 2500;
        Jump_pitch = Pitch1;
        // rt_mb_send(buzzer_mailbox, 50);
    }

    if (Jump_Delay >= 0)
        Jump_Delay--;

    if (Jump_Delay_flag >= 0)
        Jump_Delay_flag--;
    else
        Jump_flag = 0;

    if (length_val < 120 && Jump_flag) {
        if ((Jump_pitch - Pitch1) >= 5) {
            if (Straight_Counter > 80)
                Jump_flag = 0;
            type_delay = TYPE_DELAY;
            Jump_Delay = 2000;
            // rt_mb_send(buzzer_mailbox, 50);
        }
    }





    //圆环延时
    Ring_Delay();
    //速度环
    //   if(start_flag)
//    {
////        square_signal();
//        if(0 == (time%5))
//        {
//            //采集编码器数据
//            Encoder_GetCounter();
//            speed_control();
//        }
//        if(run_flag || !out_garage)
//           Motor_Control_Run( motor_r_duty*protect_flag);
//
//        wofa_data[0] =motor_l.target_speed;
//        wofa_data[1]= motor_l.current_speed;
//        wofa_data[2]= motor_r.current_speed;

    //  }
}


//void timer_pit_init(void)
//{
//    rt_timer_t timer;
//
//    //创建一个定时器 周期运行
//    timer = rt_timer_create("timer1", timer1_pit_entry, RT_NULL, 1, RT_TIMER_FLAG_PERIODIC);
//    //创建一个动态定时器，1个工作节拍
//    //启动定时器
//    if(RT_NULL != timer)
//    {
//        rt_timer_start(timer);
//    }
//}
//

/*调试用方波生成
 *
 */
int32 clk;

//void square_signal(void)
//{
//    clk++;
//    if(clk > 10000) clk = 0;
//    if (clk < 2000) {
//        motor_l.target_speed = 10;
//        motor_r.target_speed = 10;
//    } else if (clk < 4000) {
//        motor_l.target_speed = 40;
//        motor_r.target_speed = 40;
//    } else if (clk < 6000) {
//        motor_l.target_speed = 60;
//        motor_r.target_speed = 60;
//    } else if (clk < 8000) {
//        motor_l.target_speed = 40;
//        motor_r.target_speed = 40;
//    } else if (clk < 10000) {
//        motor_l.target_speed = 0;
//        motor_r.target_speed = 0;
//    }
//}
/*
 *
 *
 * 圆环延时
 */
void Ring_Delay(void) {

    if (R_Ring_first_White || L_Ring_first_White) {
        count_Ring_judge++;
        if (count_Ring_judge >= 300) {
            R_Ring_first_White = 0;
            L_Ring_first_White = 0;
            count_Ring_judge = 0;
        }
    } else {
        count_Ring_judge = 0;
    }


    if (Right_Ring_flag || Left_Ring_flag) {
        count_Ring_judge2++;
        if (count_Ring_judge2 >= 3000) {
            Right_Ring_flag = 0;
            Left_Ring_flag = 0;
            count_Ring_judge2 = 0;
        }
    }
    else count_Ring_judge2 = 0;

}
