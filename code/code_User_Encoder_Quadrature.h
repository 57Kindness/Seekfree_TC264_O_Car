//
// Created by 57Kindness on 2023/3/1.
//

#ifndef CODE_USER_ENCODER_QUADRATURE_H
#define CODE_USER_ENCODER_QUADRATURE_H

#include "zf_common_headfile.h"
#include "code_User_headfile.h"
#include "isr_config.h"


sint16 Encoder_GetCounter(void);

sint16 Left_FlyWheel_Encoder_GetCounter(void);

sint16 Right_FlyWheel_Encoder_GetCounter(void);

#endif //SEEKFREE_TC264_OPENSOURCE_LIBRARY_CODE_USER_ENCODER_QUADRATURE_H
