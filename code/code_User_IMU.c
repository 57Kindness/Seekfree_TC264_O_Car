//
// Created by 57Kindness on 2023/2/27.
//

#include "code_User_IMU.h"


float Pitch1, Roll1, Yaw1;
float Accel_x, Accel_y, Accel_z, Gyro_x, Gyro_y, Gyro_z;
float ax, ay, az;
float Angle_x_temp, Angle_y_temp, Angle_z_temp;
float Angle_X_Final, Angle_Y_Final, Angle_Z_Final;
extern signed short icm20602_acc_x, icm20602_acc_y, icm20602_acc_z;            //加速度传感器原始数据
extern signed short icm20602_gyro_x, icm20602_gyro_y, icm20602_gyro_z;         //陀螺仪原始数据


void Kalman_Filter_Euler(void) {
    icm20602_get_acc();
    icm20602_get_gyro();

    Accel_x = icm20602_acc_x;
    Accel_y = icm20602_acc_y;
    Accel_z = icm20602_acc_z;
    Gyro_x = icm20602_gyro_x;
    Gyro_y = icm20602_gyro_y;
    Gyro_z = icm20602_gyro_z;

    ax = (9.8 * Accel_x) / 8192;
    ay = (9.8 * Accel_y) / 8192;
    az = (9.8 * Accel_z) / 8192;

    Gyro_x = (Gyro_x) / 16.4;
    Gyro_y = (Gyro_y) / 16.4;
    Gyro_z = (Gyro_z) / 16.4;

    //用加速度计算三个轴和水平面坐标系之间的夹角
    Angle_x_temp = (atan(ay / az)) * 180 / 3.14;
    Angle_y_temp = (atan(ax / az)) * 180 / 3.14;
    Angle_z_temp = (atan(ay / ax)) * 180 / 3.14;

    //卡尔曼滤波计算X倾角
    Kalman_Filter_X(Angle_x_temp, Gyro_x);
    //卡尔曼滤波计算Y倾角
    Kalman_Filter_Y(Angle_y_temp, Gyro_y);
    //卡尔曼滤波计算Z倾角
    Kalman_Filter_Z(Angle_z_temp, Gyro_z);


}


//卡尔曼参数
float Q_angle = 0.001;
float Q_gyro = 0.003;
float IMU_R_angle = 0.5;
float dt = 0.002;//dt为kalman滤波器采样时间
char C_0 = 1;
float Q_bias, Angle_err;
float PCt_0, PCt_1, E;
float K_0, K_1, t_0, t_1;
float Pdot[4] = {0, 0, 0, 0};
float PP[2][2] = {{1, 0},
                  {0, 1}};


/**************************************************************************
卡尔曼滤波算法（ X 角）
**************************************************************************/
void Kalman_Filter_X(float Accel, float Gyro) //卡尔曼函数
{
    Angle_X_Final += (Gyro - Q_bias) * dt; //先验估计

    Pdot[0] = Q_angle - PP[0][1] - PP[1][0]; // Pk-先验估计误差协方差的微分

    Pdot[1] = -PP[1][1];
    Pdot[2] = -PP[1][1];
    Pdot[3] = Q_gyro;

    PP[0][0] += Pdot[0] * dt;   // Pk-先验估计误差协方差微分的积分
    PP[0][1] += Pdot[1] * dt;   // =先验估计误差协方差
    PP[1][0] += Pdot[2] * dt;
    PP[1][1] += Pdot[3] * dt;


    Angle_err = Accel - Angle_X_Final;  //zk-先验估计

    PCt_0 = C_0 * PP[0][0];
    PCt_1 = C_0 * PP[1][0];

    E = IMU_R_angle + C_0 * PCt_0;

    K_0 = PCt_0 / E;
    K_1 = PCt_1 / E;
    t_0 = PCt_0;
    t_1 = C_0 * PP[0][1];

    PP[0][0] -= K_0 * t_0;       //后验估计误差协方差
    PP[0][1] -= K_0 * t_1;
    PP[1][0] -= K_1 * t_0;
    PP[1][1] -= K_1 * t_1;

    Angle_X_Final += K_0 * Angle_err;    //后验估计
    Q_bias += K_1 * Angle_err;    //后验估计
    Gyro_x = Gyro - Q_bias;  //输出值（后验估计）的微分 = 角速度
    Pitch1 = Angle_X_Final;
}


/**************************************************************************
卡尔曼滤波算法（ Y 角）
**************************************************************************/
void Kalman_Filter_Y(float Accel, float Gyro) //卡尔曼函数
{
    Angle_Y_Final += (Gyro - Q_bias) * dt; //先验估计

    Pdot[0] = Q_angle - PP[0][1] - PP[1][0]; // Pk-先验估计误差协方差的微分

    Pdot[1] = -PP[1][1];
    Pdot[2] = -PP[1][1];
    Pdot[3] = Q_gyro;

    PP[0][0] += Pdot[0] * dt;   // Pk-先验估计误差协方差微分的积分
    PP[0][1] += Pdot[1] * dt;   // =先验估计误差协方差
    PP[1][0] += Pdot[2] * dt;
    PP[1][1] += Pdot[3] * dt;


    Angle_err = Accel - Angle_Y_Final;  //zk-先验估计

    PCt_0 = C_0 * PP[0][0];
    PCt_1 = C_0 * PP[1][0];

    E = IMU_R_angle + C_0 * PCt_0;

    K_0 = PCt_0 / E;
    K_1 = PCt_1 / E;

    t_0 = PCt_0;
    t_1 = C_0 * PP[0][1];

    PP[0][0] -= K_0 * t_0;       //后验估计误差协方差
    PP[0][1] -= K_0 * t_1;
    PP[1][0] -= K_1 * t_0;
    PP[1][1] -= K_1 * t_1;

    Angle_Y_Final += K_0 * Angle_err;  //后验估计
    Q_bias += K_1 * Angle_err;  //后验估计
    Gyro_y = Gyro - Q_bias;    //输出值（后验估计）的微分 = 角速度
    Roll1 = Angle_Y_Final;
}

/**************************************************************************
卡尔曼滤波算法（ Z 角）
**************************************************************************/
void Kalman_Filter_Z(float Accel, float Gyro) {
    //先验估计
    Angle_Z_Final += (Gyro - Q_bias) * dt;

    // Pk - 先验估计误差协方差的微分
    Pdot[0] = Q_angle - PP[0][1] - PP[1][0];
    Pdot[1] = -PP[1][1];
    Pdot[2] = -PP[1][1];
    Pdot[3] = Q_gyro;

    // Pk - 先验估计误差协方差微分的积分
    PP[0][0] += Pdot[0] * dt;

    // = 先验估计误差协方差
    PP[0][1] += Pdot[1] * dt;
    PP[1][0] += Pdot[2] * dt;
    PP[1][1] += Pdot[3] * dt;

    // zk - 先验估计
    Angle_err = Accel - Angle_Z_Final;

    PCt_0 = C_0 * PP[0][0];
    PCt_1 = C_0 * PP[1][0];

    E = IMU_R_angle + C_0 * PCt_0;

    K_0 = PCt_0 / E;
    K_1 = PCt_1 / E;

    t_0 = PCt_0;
    t_1 = C_0 * PP[0][1];

    //后验估计误差协方差
    PP[0][0] -= K_0 * t_0;
    PP[0][1] -= K_0 * t_1;
    PP[1][0] -= K_1 * t_0;
    PP[1][1] -= K_1 * t_1;

    //后验估计
    Angle_Z_Final += K_0 * Angle_err;

    //后验估计
    Q_bias += K_1 * Angle_err;

    //输出值（后验估计）的微分 = 角速度
    Gyro_z = Gyro - Q_bias;
    Yaw1 = Angle_Z_Final;
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     偏差角度计算
// 参数说明
// 返回参数
// 使用示例
// 备注信息
// 备注信息
// 作者信息     Created by 57Kindness on 2023/4/15.
//-------------------------------------------------------------------------------------------------------------------
float angle_diff(float a1, float a2) {
    float c1 = cosf(a1 / 180 * 3.14);
    float s1 = sinf(a1 / 180 * 3.14);
    float c2 = cosf(a2 / 180 * 3.14);
    float s2 = sinf(a2 / 180 * 3.14);
    float c = c1 * c2 + s1 * s2;
    float s = s1 * c2 - s2 * c1;
    return atan2f(s, c) * 180 / 3.14;
}

