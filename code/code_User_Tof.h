//
// Created by 57Kindness on 2023/6/15.
//

#ifndef O_CAR_DEMO_CODE_USER_TOF_H
#define O_CAR_DEMO_CODE_USER_TOF_H

#include "code_User_headfile.h"
#include "zf_common_headfile.h"

#define TOF_UART UART_3

extern int length_val;
extern char ch_2019[16];


void tof_receive(void);

void tof_handle(void);


#endif //O_CAR_DEMO_CODE_USER_TOF_H
