#include "zf_common_headfile.h"
#include "code_User_headfile.h"


float vofa_data[5];
float ftemp[5];
uint8_t temp[20];

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     vofa+发送协议
// 参数说明
// 返回参数
// 使用示例     vofa_Send_Data(vofa_data[0],vofa_data[1],vofa_data[2],vofa_data[3]);
// 备注信息
// 作者信息     Created by 57Kindness on 2023/3/1.
//-------------------------------------------------------------------------------------------------------------------
void vofa_Send_Data(float data1, float data2, float data3, float data4) {

    ftemp[0] = data1;
    ftemp[1] = data2;
    ftemp[2] = data3;
    ftemp[3] = data4;
    //ftemp[4]=data5;
    memcpy(temp, (uint8_t *) ftemp, sizeof(ftemp));
    temp[16] = 0x00;                    //写入尾数据
    temp[17] = 0x00;
    temp[18] = 0x80;
    temp[19] = 0x7f;
    // uart_putbuff(UART_0,&temp[0],16);
    //wireless_uart_send_buff(temp,16);
    uart_write_buffer(UART_2, (const uint8 *) &temp, 20);
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     vofa+
// 参数说明
// 返回参数
// 使用示例     vofa_Send();
// 备注信息
// 作者信息     Created by 57Kindness on 2023/5/25.
//-------------------------------------------------------------------------------------------------------------------
void vofa_Send(void) {
    vofa_data[0] = (float) Palst_L.Out;
    vofa_data[1] = (float) Palst_R.Out;
    vofa_data[2] = (float) 0;
    vofa_data[3] = (float) 9;
    vofa_Send_Data(vofa_data[0], vofa_data[1], vofa_data[2], vofa_data[3]);
}
















