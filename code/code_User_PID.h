//
// Created by 57Kindness on 2023/2/27.
//

#ifndef CODE_USER_PID_H_
#define CODE_USER_PID_H_

#include "zf_common_headfile.h"
#include "code_User_headfile.h"

typedef struct {
    float Kp;
    float Ki;
    float Kd;
    float Err;
    float Err_last;
    float Err_l2st;  //前前次误差
    float Integral_lim; //积分限幅
    float Err_Int;  //误差积分
    float P_out;
    float I_out;
    float D_out;
    float Out;
    float Out_lim;
} PID;

extern PID Palst_R;
extern PID Palst_L;
extern PID Angle_R;
extern PID Angle_L;
extern PID Speed_R;
extern PID Speed_L;
extern PID Balance;
extern PID Vec;
extern PID Turn;

float Pid_Position(float Target, float Feedback, PID *Pid);

float Pid_Incre(float Target, float Feedback, PID *Pid);

#endif
