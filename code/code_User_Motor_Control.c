//
// Created by 57Kindness on 2023/2/28.
//

#include "code_User_Motor_Control.h"

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     Motor 初始化
// 参数说明
// 返回参数
// 使用示例     Motor_Control_Int ();
// 备注信息     此函数仅适用drv8701e单路驱动，需要其他驱动需要找历程库重新移植
// 作者信息     Created by 57Kindness on 2023/2/28.
//-------------------------------------------------------------------------------------------------------------------
void Motor_Control_Int(void) {
    gpio_init(MotorDIR, GPO, GPIO_HIGH, GPO_PUSH_PULL);                             // GPIO 初始化为输出 默认上拉输出高
    pwm_init(MotorPWM, 17000, 1);                                                   // PWM 通道初始化频率 17KHz 占空比初始为 0
    encoder_quad_init(ENCODER_1, ENCODER_1_A, ENCODER_1_B);                         // 初始化编码器模块与引脚 正交解码编码器模式
}


//-------------------------------------------------------------------------------------------------------------------
// 函数简介     Motor 开转
// 参数说明
// 返回参数
// 使用示例
// 备注信息
// 作者信息     Created by 57Kindness on 2023/2/28.
//-------------------------------------------------------------------------------------------------------------------
void Motor_Run(int duty) {
    if (duty >= 0) {
        gpio_set_level(MotorDIR, GPIO_HIGH);
        pwm_set_duty(MotorPWM, duty);
    } else {
        gpio_set_level(MotorDIR, GPIO_LOW);
        pwm_set_duty(MotorPWM, duty);
    }
}
