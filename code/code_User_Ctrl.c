//
// Created by 57Kindness on 2023/2/27.
//

#include <code_User_Ctrl.h>

//机械零点  (前进)
float Angle_Mot = -1.30;
//机械零点  (动量轮)
float Angle_Fly = -3.54;
//动态零点
float Angle_zero3 = 0;

//角速度环结构体
PID Palst_R;
PID Palst_L;
//角度环结构体
PID Angle_R;
PID Angle_L;
//速度环结构体
PID Speed_R;
PID Speed_L;
//行进轮直立环PID
PID Balance;
PID Vec;
//转向环PID
PID Turn;

//角速度环 角度环 速度环计算周期变量
unsigned char g_t, a_t, v_t;

//摔倒保护  启停保护
unsigned char protect_flag=1, Start_Flag = 0;

//期望速度
float Set_Speed = 0;

//平滑变量
float Turn_Ending_Now = 0 ,Turn_Ending_Last = 0 , Turn_increment = 0 , Turn_Zero = 0;

//摄像头中线偏差承接变量
int16 Turn_Difference = 0;

//最终输出PWM的承接变量
sint16 Duty_L, Duty_R;
int Motro_Duty;

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     串级总体控制
// 参数说明
// 返回参数
// 使用示例     Balance_All();
// 备注信息
// 作者信息     Created by 57Kindness on 2023/4/15.
//-------------------------------------------------------------------------------------------------------------------
void Balance_All(void) {
    g_t++;
    a_t++;
    v_t++;

    //2ms周期
    if (g_t == 2) {
        g_t = 0;
        Kalman_Filter_Euler();
        Palst_loop_All();                        //动量轮角速度环PID
        Balance_Ctrl();                          //行进轮 直立环PID

        //计算占空比
        if(protect_flag == 1 && Start_Flag == 1) {
            Duty_L = Palst_L.Out + Turn.Out;
            Duty_R = Palst_R.Out - Turn.Out;
            Motro_Duty = (int) (Balance.Out - (Balance.Kp * Vec.Out));
        }
        //触发保护进入抱死
        else {
            Duty_L = -Left_FlyWheel_Encoder_GetCounter() * 1000;
            Duty_R = Right_FlyWheel_Encoder_GetCounter() * 1000;
            Motro_Duty = 0;
        }

        //死区
        if (Duty_L < -0)Duty_L -= 700;
        else if (Duty_L > 0)Duty_L += 700;
        if (Duty_R < -0)Duty_R -= 700;
        else if (Duty_R > 0)Duty_R += 700;

        if (Motro_Duty < -0)Motro_Duty -= 300;
        else if (Motro_Duty > 0)Motro_Duty += 300;

        //电机保护
        if(Motro_Duty >  3500) Motro_Duty =  3500;
        if(Motro_Duty < -3500) Motro_Duty = -3500;

        if(Duty_L >  9500) Duty_L =  9500;
        if(Duty_L < -9500) Duty_L = -9500;

        if(Duty_R >  9500) Duty_R =  9500;
        if(Duty_R < -9500) Duty_R = -9500;

        if(func_abs(Roll1- Angle_Fly)>5 || func_abs(Pitch1- Angle_Mot)>10) protect_flag=0;
        else protect_flag=1;
    }

    //8ms周期
    if (a_t == 8) {
        a_t = 0;
        Angle_loop_All();                        //动量轮角度环
        Vec_Ctrl();                              //行进轮速度环
    }

    //20ms周期
    if (v_t == 20) {
        v_t = 0;
        Speed_loop_All();                        //动量轮速度环
        Turn_loop((float)difference);            //转向环
    }

    //占空比输出
    FlyWheel_All_Run(-Duty_L, Duty_R);
    if(Motro_Duty >= 0)
    {
        gpio_set_level(MotorDIR, GPIO_HIGH);
        pwm_set_duty(MotorPWM, Motro_Duty);
    }
    else
    {
        gpio_set_level(MotorDIR, GPIO_LOW);
        pwm_set_duty(MotorPWM, -Motro_Duty);
    }
}




/*********************************飞轮控制*********************************/
//-------------------------------------------------------------------------------------------------------------------
// 函数简介     双飞轮角速度环控制
// 参数说明
// 返回参数
// 使用示例     Palst_loop_All();
// 备注信息     主要由PD控制
// 备注信息     角速度环提供翻滚时的阻尼感
// 作者信息     Created by 57Kindness on 2023/4/15.
//-------------------------------------------------------------------------------------------------------------------

void Palst_loop_All(void) {
    //输入为角度环输出
//    Pid_Position(Angle_L.Out + Turn.Kp , 0.7 * icm20602_gyro_y, &Palst_L);
//    Pid_Position(Angle_R.Out - Turn.Kp , 0.7 * icm20602_gyro_y, &Palst_R);
    Pid_Position(Angle_L.Out, 0.7 * icm20602_gyro_y, &Palst_L);
    Pid_Position(Angle_R.Out, 0.7 * icm20602_gyro_y, &Palst_R);
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     双飞轮角度环控制
// 参数说明
// 返回参数
// 使用示例     Angle_loop_All();
// 备注信息     主要由PD控制
// 备注信息     角度环提供滚时角回到机械零点的回复力
// 作者信息     Created by 57Kindness on 2023/4/15.
//-------------------------------------------------------------------------------------------------------------------
void Angle_loop_All(void) {
    //输入为速度环输出减去机械零点减去动态零点
    Pid_Position(0.1 * Speed_L.Out - Angle_Fly - Angle_zero3 * 0.01, Roll1 , &Angle_L);
    Pid_Position(0.1 * Speed_L.Out - Angle_Fly - Angle_zero3 * 0.01, Roll1 , &Angle_R);
//    Pid_Position(0.1 * Speed_L.Out - Angle_Fly - Angle_zero3 * 0.01, eulerAngle.roll , &Angle_L);
//    Pid_Position(0.1 * Speed_L.Out - Angle_Fly - Angle_zero3 * 0.01, eulerAngle.roll , &Angle_R);
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     双飞轮速度环继承函数
// 参数说明
// 返回参数
// 使用示例     Speed_loop_All();
// 备注信息     主要由PI控制
// 作者信息     Created by 57Kindness on 2023/4/15.
//-------------------------------------------------------------------------------------------------------------------
void Speed_loop_All(void) {
    sint16 ECPULSE_L;
    static float Encoder_Least_L;

    sint16 ECPULSE_R;
    static float Encoder_Least_R;

    ECPULSE_L = Left_FlyWheel_Encoder_GetCounter();
    ECPULSE_R = Right_FlyWheel_Encoder_GetCounter();

    ECPULSE_L *= 0.7;
    ECPULSE_L += Encoder_Least_L * 0.3;

    ECPULSE_R *= 0.7;
    ECPULSE_R += Encoder_Least_R * 0.3;

    Pid_Incre( 0,ECPULSE_L * 0.5 , &Speed_L);
    Pid_Incre( 0,ECPULSE_R * 0.5 , &Speed_R);

    Encoder_Least_L = ECPULSE_L;
    Encoder_Least_R = ECPULSE_R;
}

/*
//-------------------------------------------------------------------------------------------------------------------
// 函数简介     速度环计算
// 参数说明     ECPULSE 飞轮编码器值获取    PID *temp 运算PID地址
// 返回参数
// 使用示例     Speed_loop_Count(Left_FlyWheel_Encoder_GetCounter(), &Speed_L);
// 使用示例     Speed_loop_Count(Right_FlyWheel_Encoder_GetCounter(), &Speed_R);
// 备注信息
// 作者信息     Created by 57Kindness on 2023/4/15.
//-------------------------------------------------------------------------------------------------------------------
void Speed_loop_Count(int ECPULSE,PID *temp) {
    float Encoder_Least;
    static float Encoder;
    Encoder_Least = ECPULSE;
    //一阶互补滤波
    Encoder *= 0.7;
    Encoder += Encoder_Least * 0.3;
    //Target为0  输入为编码器采集值
    Pid_Position(0, Encoder, temp);
}
*/

/*********************************行进轮控制*********************************/

//直立环
void Balance_Ctrl(void) {
    Balance.Out = Balance.Kp * (Pitch1 - Angle_Mot) + Balance.Kd * icm20602_gyro_x;
//    Balance.Out = Balance.Kp * (eulerAngle.pitch - Angle_Mot) + Balance.Kd * icm20602_gyro_x;
}

//速度环
void Vec_Ctrl(void) {
    sint16 Encoder2;
    static float Encoder_Least2;
    Encoder2 = Encoder_GetCounter();
    Encoder2 *= 0.7;
    Encoder2 += Encoder_Least2 * 0.3;
    Pid_Position(Set_Speed, Encoder2, &Vec);
    Encoder_Least2 = Encoder2;
}


/********************************转向环*******************************/

//传入形参为赛道中线与车身偏差
void Turn_loop(float Offset) {
    Pid_Position(10, Yaw1, &Turn);
    Angle_zero3 = Offset * Turn.Kd;
}


/********************************缓冲环*******************************/
//-------------------------------------------------------------------------------------------------------------------
// 函数简介     飞轮转向缓冲环
// 参数说明     target: 目标值  target_last: 上一次的目标值   increment: 每次递增/递减的步长
// 返回参数     平滑逼近后的目标值
// 使用示例
// 备注信息
// 作者信息     Created by 57Kindness on 2023/5/29.
//-------------------------------------------------------------------------------------------------------------------
float smoothTransition(float target, float target_last, float increment) {
    // 如果目标值与上一次的目标值相等，则无需逼近，直接返回目标值
    if (target == target_last) {
        return target;
    }
    // 计算逼近的方向（增加或减小）
    int direction = (target > target_last) ? 1 : -1;
    // 持续逼近直到达到目标值
    while (target_last != target) {
        // 根据逼近的方向更新上一次的目标值
        target_last += increment * direction;
        // 如果逼近超过目标值，则将上一次的目标值设置为目标值
        if ((direction == 1 && target_last > target) || (direction == -1 && target_last < target)) {
            target_last = target;
        }
        Turn_Ending_Now =target_last;
    }
    // 返回平滑逼近后的目标值
    return target_last;
}


/*********************************PID初始化*********************************/
//PID参数初始化
void PID_Init(void) {
/*********************************角速度环参数*********************************/
/*********************************角速度环参数*********************************/
    Palst_R.Kp = 0;
    Palst_R.Ki = 0;
    Palst_R.Kd = 0;
    Palst_R.Err = 0;
    Palst_R.Err_last = 0;
    Palst_R.Err_l2st = 0;
    Palst_R.Integral_lim = 3000;   //3000
    Palst_R.Err_Int = 0;
    Palst_R.P_out = 0;
    Palst_R.I_out = 0;
    Palst_R.D_out = 0;
    Palst_R.Out = 0;
    Palst_R.Out_lim = 9000;     //9000 5000

    Palst_L.Kp = 0;
    Palst_L.Ki = 0;
    Palst_L.Kd = 0;
    Palst_L.Err = 0;
    Palst_L.Err_last = 0;
    Palst_L.Err_l2st = 0;
    Palst_L.Integral_lim = 3000;   //3000
    Palst_L.Err_Int = 0;
    Palst_L.P_out = 0;
    Palst_L.I_out = 0;
    Palst_L.D_out = 0;
    Palst_L.Out = 0;
    Palst_L.Out_lim = 9000;     //9000 5000
/*********************************角速度环参数*********************************/
/*********************************角速度环参数*********************************/

/*********************************角度环参数*********************************/
/*********************************角度环参数*********************************/
    Angle_R.Kp = 0; //-
    Angle_R.Ki = 0; //
    Angle_R.Kd = 0; //-
    Angle_R.Err = 0;
    Angle_R.Err_last = 0;
    Angle_R.Err_l2st = 0;
    Angle_R.Integral_lim = 1000;
    Angle_R.Err_Int = 0;
    Angle_R.P_out = 0;
    Angle_R.I_out = 0;
    Angle_R.D_out = 0;
    Angle_R.Out = 0;
    Angle_R.Out_lim = 9000;

    Angle_L.Kp = 0;//+
    Angle_L.Ki = 0;//
    Angle_L.Kd = 0;//+(减小回正阻尼)
    Angle_L.Err = 0;
    Angle_L.Err_last = 0;
    Angle_L.Err_l2st = 0;
    Angle_L.Integral_lim = 1000;
    Angle_L.Err_Int = 0;
    Angle_L.P_out = 0;
    Angle_L.I_out = 0;
    Angle_L.D_out = 0;
    Angle_L.Out = 0;
    Angle_L.Out_lim = 9000;
/*********************************角度环参数*********************************/
/*********************************角度环参数*********************************/

/*********************************速度环参数*********************************/
/*********************************速度环参数*********************************/
    Speed_R.Kp = 0;//-
    Speed_R.Ki = 0; //-
    Speed_R.Kd = 0; //
    Speed_R.Err = 0;
    Speed_R.Err_last = 0;
    Speed_R.Err_l2st = 0;
    Speed_R.Integral_lim = 200;
    Speed_R.Err_Int = 0;
    Speed_R.P_out = 0;
    Speed_R.I_out = 0;
    Speed_R.D_out = 0;
    Speed_R.Out = 0;
    Speed_R.Out_lim = 500;

    Speed_L.Kp = 0; //-
    Speed_L.Ki = 0; //-
    Speed_L.Kd = 0; //
    Speed_L.Err = 0;
    Speed_L.Err_last = 0;
    Speed_L.Err_l2st = 0;
    Speed_L.Integral_lim = 200;
    Speed_L.Err_Int = 0;
    Speed_L.P_out = 0;
    Speed_L.I_out = 0;
    Speed_L.D_out = 0;
    Speed_L.Out = 0;
    Speed_L.Out_lim = 500;
/*********************************速度环参数*********************************/
/*********************************速度环参数*********************************/

    /******角度环参数**********/
    Balance.Kp = 0; //+       //20
    Balance.Ki = 0; //
    Balance.Kd = 0; //+
    Balance.Err = 0;
    Balance.Err_last = 0;
    Balance.Err_l2st = 0;
    Balance.Integral_lim = 0;
    Balance.Err_Int = 0;
    Balance.P_out = 0;
    Balance.I_out = 0;
    Balance.D_out = 0;
    Balance.Out = 0;
    Balance.Out_lim = 5000;

    /**********速度环参数*********/
    Vec.Kp = 0; //+       //20
    Vec.Ki = 0; //
    Vec.Kd = 0; //+
    Vec.Err = 0;
    Vec.Err_last = 0;
    Vec.Err_l2st = 0;
    Vec.Integral_lim = 0;
    Vec.Err_Int = 0;
    Vec.P_out = 0;
    Vec.I_out = 0;
    Vec.D_out = 0;
    Vec.Out = 0;
    Vec.Out_lim = 5000;

    /*****转向环参数****************/       //基本上纯P就够
    Turn.Kp = 0; //+       //20
    Turn.Ki = 0; //
    Turn.Kd = 0; //+
    Turn.Err = 0;
    Turn.Err_last = 0;
    Turn.Err_l2st = 0;
    Turn.Integral_lim = 0;
    Turn.Err_Int = 0;
    Turn.P_out = 0;
    Turn.I_out = 0;
    Turn.D_out = 0;
    Turn.Out = 0;
    Turn.Out_lim = 1200;

}

