//
// Created by 57Kindness on 2023/2/28.
//

#ifndef CODE_USER_MOTOR_CONTROL_H
#define CODE_USER_MOTOR_CONTROL_H

#include "zf_common_headfile.h"
#include "code_User_headfile.h"

#define MotorDIR                  (P21_4)
#define MotorPWM                  (ATOM0_CH3_P21_5)

#define ENCODER_1                 (TIM6_ENCODER)
#define ENCODER_1_A               (TIM6_ENCODER_CH1_P20_3)
#define ENCODER_1_B               (TIM6_ENCODER_CH2_P20_0)

void Motor_Control_Int(void);
void Motor_Run(int duty);

#endif //SEEKFREE_TC264_OPENSOURCE_LIBRARY_CODE_USER_MOTOR_CONTROL_H
