//
// Created by 57Kindness on 2023/5/25.
//

#include "code_User_IPS_Show.h"

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     屏幕显示控制函数
// 参数说明
// 返回参数
// 使用示例     Ips_Show();
// 备注信息
// 作者信息     Created by 57Kindness on 2023/5/25.
//-------------------------------------------------------------------------------------------------------------------
void Ips_Show(void) {
    //菜单显示部分
    if (!gpio_get_level(P33_11) && gpio_get_level(P33_9)) {
        menu_entry();
        display_entry();
    }
    //摄像头显示部分
    if (!gpio_get_level(P33_9) && gpio_get_level(P33_11)) {
        if (!gpio_get_level(P33_13)) {

            ips200_show_gray_image(0, 0, ((const uint8 *) mt9v03x_image), MT9V03X_W, MT9V03X_H, (MT9V03X_W),
                                   (MT9V03X_H), image_threshold);
            show_line();
            show_add_line();
        } else {
            ips200_displayimage03x((const uint8 *) mt9v03x_image, MT9V03X_W, MT9V03X_H);
            show_line();
            show_add_line();
        }

    }
    //参数显示
    if (!gpio_get_level(P33_12)) {
        //x方向一个字符占8位 y方向一个字符16位
        ips200_show_string(0  , 220, "M");
        ips200_show_float (16 , 220, Pitch1 - Angle_Mot, 3, 2);
        ips200_show_string(72 , 220, "F");
        ips200_show_float (88 , 220, Roll1 - Angle_Fly, 3, 2);
        ips200_show_string(144, 220, "Y");
        ips200_show_float (160, 220, Yaw1, 3, 2);


        ips200_show_string(0  , 236, "PL");
        ips200_show_float (32 , 236, Palst_L.Out, 3, 2);
        ips200_show_string(150, 236, "PR");
        ips200_show_float (182, 236, Palst_R.Out, 3, 2);

        ips200_show_string(0  , 252, "AL");
        ips200_show_float (32 , 252, Angle_L.Out, 3, 2);
        ips200_show_string(150, 252, "AR");
        ips200_show_float (182, 252, Angle_R.Out, 3, 2);

        ips200_show_string(0  , 268, "SL");
        ips200_show_float (32 , 268, Speed_L.Out, 3, 2);
        ips200_show_string(150, 268, "SR");
        ips200_show_float (182, 268, Speed_R.Out, 3, 2);

        ips200_show_string(0  , 284, "Bal");
        ips200_show_float (32 , 284, Balance.Out, 3, 2);
        ips200_show_string(150, 284, "Vec");
        ips200_show_float (182, 284, Vec.Out, 3, 2);

        ips200_show_string(0  , 300, "off");
        ips200_show_int   (32 , 300, Turn_Difference, 3);
        ips200_show_string(150, 300, "Tur");
        ips200_show_float (182, 300, Turn.Out, 3, 2);


    }
    //保护部分
    if (!gpio_get_level(P32_4)) Start_Flag = 0;
    else Start_Flag = 1;
}
