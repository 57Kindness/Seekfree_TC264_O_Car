//
// Created by 57Kindness on 2023/2/27.
//

#ifndef CODE_USER_LEFT_FLYWHEEL_H
#define CODE_USER_LEFT_FLYWHEEL_H

#include "zf_common_headfile.h"
#include "code_User_headfile.h"

#define LEFT_FLYWHEEL_PWM               (ATOM0_CH7_P02_7        )     // ������PWM��������
#define LEFT_FLYWHEEL_DUTY_LEVEL        (0                      )     // ������ռ�ձ���Ч��ƽ
#define LEFT_FLYWHEEL_DIR               (P02_6                  )     // �����ַ����������
#define LEFT_FLYWHEEL_CLOCKWISE         (1                      )     // ������˳ʱ����ת����
#define LEFT_FLYWHEEL_BRAKE             (P11_3                  )     // ������ɲ����������
#define LEFT_FLYWHEEL_ENCODER_INDEX     (TIM5_ENCODER          )     // �����ֱ��������
#define LEFT_FLYWHEEL_ENCODER_CH1       (TIM5_ENCODER_CH1_P10_3)     // �����ֱ�����ͨ��1
#define LEFT_FLYWHEEL_ENCODER_CH2       (TIM5_ENCODER_CH2_P10_1)     // �����ֱ�����ͨ��2


void Left_FlyWheel_Int(void);

void Left_FlyWheel_Run(int16 duty_out);

#endif //SEEKFREE_TC264_OPENSOURCE_LIBRARY_CODE_USER_LEFT_FLYWHEEL_H
