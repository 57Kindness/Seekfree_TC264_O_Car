//
// Created by 57Kindness on 2023/2/27.
//
#include "code_User_FlyWheel_AllControl.h"

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     Left_FlyWheel 初始化
// 参数说明     Duty_L:左飞轮轮占空  Duty_R:右飞轮占空比
// 返回参数
// 使用示例     FlyWheel_All_Run (5000,5000);
// 备注信息
// 作者信息     Created by 57Kindness on 2023/3/30.
//-------------------------------------------------------------------------------------------------------------------
void FlyWheel_All_Run(int16 Duty_L, int16 Duty_R) {
    Left_FlyWheel_Run(Duty_L);
    Right_FlyWheel_Run(Duty_R);
}

