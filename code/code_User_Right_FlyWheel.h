//
// Created by 57Kindness on 2023/2/27.
//

#ifndef CODE_USER_RIGHT_FLYWHEEL_H
#define CODE_USER_RIGHT_FLYWHEEL_H

#include "zf_common_headfile.h"
#include "code_User_headfile.h"

#define RIGHT_FLYWHEEL_PWM              (ATOM0_CH5_P02_5        )     // �Ҳ����PWM��������
#define RIGHT_FLYWHEEL_DUTY_LEVEL       (0                      )     // �Ҳ����ռ�ձ���Ч��ƽ
#define RIGHT_FLYWHEEL_DIR              (P02_4                  )     // �Ҳ���ַ����������
#define RIGHT_FLYWHEEL_CLOCKWISE        (1                      )     // �Ҳ����˳ʱ����ת����
#define RIGHT_FLYWHEEL_BRAKE            (P11_2                  )     // �Ҳ����ɲ����������
#define RIGHT_FLYWHEEL_ENCODER_INDEX    (TIM2_ENCODER          )     // �Ҳ���ֱ��������
#define RIGHT_FLYWHEEL_ENCODER_CH1      (TIM2_ENCODER_CH1_P33_7)     // �Ҳ���ֱ�����ͨ��1
#define RIGHT_FLYWHEEL_ENCODER_CH2      (TIM2_ENCODER_CH2_P33_6)     // �Ҳ���ֱ�����ͨ��2

void Right_FlyWheel_Int(void);

void Right_FlyWheel_Run(int16 duty_out);

#endif //SEEKFREE_TC264_OPENSOURCE_LIBRARY_CODE_USER_RIGHT_FLYWHEEL_H
