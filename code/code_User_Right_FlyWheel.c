//
// Created by 57Kindness on 2023/2/27.
//
#include "code_User_Right_FlyWheel.h"

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     Right_FlyWheel 初始化
// 参数说明
// 返回参数
// 使用示例     Right_FlyWheel_Int ();
// 备注信息
// 作者信息     Created by 57Kindness on 2023/2/27.
//-------------------------------------------------------------------------------------------------------------------
void Right_FlyWheel_Int(void) {
    pwm_init(RIGHT_FLYWHEEL_PWM, 12500,
             RIGHT_FLYWHEEL_DUTY_LEVEL == 0 ? PWM_DUTY_MAX : 0);                // 初始化右侧飞轮PWM信号
    gpio_init(RIGHT_FLYWHEEL_DIR, GPO, RIGHT_FLYWHEEL_CLOCKWISE,
              GPO_PUSH_PULL);                            // 初始化右侧飞轮DIR信号
    gpio_init(RIGHT_FLYWHEEL_BRAKE, GPO, 1,
              GPO_PUSH_PULL);                                                 // 初始化右侧飞轮刹车信号
    encoder_quad_init(RIGHT_FLYWHEEL_ENCODER_INDEX, RIGHT_FLYWHEEL_ENCODER_CH1,
                      RIGHT_FLYWHEEL_ENCODER_CH2);// 初始化右侧飞轮编码器接口

}


//-------------------------------------------------------------------------------------------------------------------
// 函数简介     Right_FlyWheel 开转
// 参数说明     duty_out :输出占空比
// 返回参数
// 使用示例     Right_FlyWheel_Run (5000);
// 备注信息
// 作者信息     Created by 57Kindness on 2023/2/27.
//-------------------------------------------------------------------------------------------------------------------
void Right_FlyWheel_Run(int16 duty_out) {
    /*int16 right_flywheel_encoder_data = 0;*/                                // 右侧飞轮编码器值

    gpio_set_level(RIGHT_FLYWHEEL_BRAKE, 1);                                     // 刹车信号取消

    if (duty_out > 0)                                                            // 根据占空比切换方向引脚
        gpio_set_level(RIGHT_FLYWHEEL_DIR, RIGHT_FLYWHEEL_CLOCKWISE);
    else
        gpio_set_level(RIGHT_FLYWHEEL_DIR, !RIGHT_FLYWHEEL_CLOCKWISE);

    pwm_set_duty(RIGHT_FLYWHEEL_PWM, 10000 - func_abs(duty_out)); // 输出占空比 占空比必须为正值 因此此处使用绝对值

}
