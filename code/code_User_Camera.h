//
// Created by 57Kindness on 2023/2/27.
//

#ifndef CODE_USER_CAMERA_H_
#define CODE_USER_CAMERA_H_

#include "zf_common_headfile.h"
#include "code_User_headfile.h"


#define LCDW 160    //图像宽度
#define LCDH 90    //图像高度
#define CountLine  89
#define MIN_LINE  10
#define MAX_LINE  150


#define MATH_PI 3.1415926

enum TYPE {
    Ring, P_l, P_r, Three_road_l, Three_road_r
};

typedef struct point {
    int16 m_i8x;
    int16 m_i8y;
} Point;
extern unsigned char Left_Add_Start, Right_Add_Start;


//出入库
extern unsigned char Starting_Line_Counter;
//圆环
extern unsigned char Left_Ring_flag;
extern unsigned char Right_Ring_flag;
extern float ring_ka, ring_kb;
extern bool R_Ring_first_White;
extern bool L_Ring_first_White;
extern unsigned char ring_num;
extern unsigned char ring_count;


//p环
extern unsigned char Left_P_flag;
extern unsigned char Right_P_flag;
extern unsigned P_direction;

//三叉
extern unsigned char Three_cross_flag;
extern unsigned char Three_cross_count;
extern unsigned char Three_cross_dir;
extern bool Three_cross_out;


/** 压缩后之后用于存放屏幕显示数据  */
extern unsigned char Image_Use[LCDH][LCDW];
extern unsigned char image_threshold;
extern unsigned char Bin_Image[LCDH][LCDW];
extern unsigned char Line_Count, Last_Line_Count;
extern unsigned char Straight_Counter;//赛道长度


//获取使用图像
void Get_Use_Image(void);

unsigned char GetOSTU(unsigned char tmImage[LCDH][LCDW]);

char Straight_judge(void);

void Get_Bin_Images(unsigned char Threshold);

void Image_Handle(unsigned char *data);

unsigned char First_Line_Handle(unsigned char *data);

unsigned char Corrode_Filter(unsigned char i, unsigned char *data, unsigned char Left_Min, unsigned char Right_Max);

unsigned char
Traversal_Black_Left(unsigned char i, unsigned char *data, unsigned char Left_Min, unsigned char Right_Max);

unsigned char
Traversal_Black_Right(unsigned char i, unsigned char *data, unsigned char Left_Min, unsigned char Right_Max);

unsigned char Traversal_Left(unsigned char i, unsigned char *data, unsigned char *Mid, unsigned char Left_Min,
                             unsigned char Right_Max);

unsigned char Traversal_Right(unsigned char i, unsigned char *data, unsigned char *Mid, unsigned char Left_Min,
                              unsigned char Right_Max);

unsigned char
Traversal_Left_Line(unsigned char i, unsigned char *data, unsigned char *Left_Line, unsigned char *Right_Line);

unsigned char
Traversal_Right_Line(unsigned char i, unsigned char *data, unsigned char *Left_Line, unsigned char *Right_Line);

void Traversal_Mid_Line(unsigned char i, unsigned char *data, unsigned char Mid, unsigned char Left_Min,
                        unsigned char Right_Max, unsigned char *Left_Line, unsigned char *Right_Line,
                        unsigned char *Left_Add_Line, unsigned char *Right_Add_Line);

unsigned char Calculate_Add(unsigned char i, float Ka, float Kb);

void Curve_Fitting(float *Ka, float *Kb, unsigned char *Start, unsigned char *Line, unsigned char *Add_Flag,
                   unsigned char Mode);

void
Line_Repair(unsigned char Start, unsigned char Stop, unsigned char *data, unsigned char *Line, unsigned char *Line_Add,
            unsigned char *Add_Flag, unsigned char Mode);

void Mid_Line_Repair(unsigned char count, unsigned char *data);

void Draw_Cross(void);

void show_add_line(void);

void show_line(void);

int16 Point_Average();

void show_image_information();

void Ring_judge(void);

void Ring_process(void);

unsigned char Slope_Left_Line(unsigned char *Left, unsigned char stop, enum TYPE type);

unsigned char Slope_Right_Line(unsigned char *Right, unsigned char stop, enum TYPE type);

void P_judge(void);

void P_process(void);

void Three_roads_judge();

void Three_roads_process();

unsigned char Special_Max_Line(unsigned char *data);

unsigned char Special_Min_Line(unsigned char *data);

void Slope_calculation(float *ka, float *kb, unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2,
                       unsigned char mode);

uint8 Get_Angle(Point A, Point B, Point C);

float OSG_Sqrt(float x);

bool Turn_calculate(void);

void Image_Process(void);

extern unsigned char foresight;
extern int16 difference;
extern uint8 image_threshold;
#endif
