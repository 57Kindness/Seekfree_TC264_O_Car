
#ifndef _vofa_H_
#define _vofa_H_

#include <stdint.h>


extern float vofa_data[5];


void vofa_Send_Data(float data1, float data2, float data3, float data4);

void vofa_Send(void);

#endif
