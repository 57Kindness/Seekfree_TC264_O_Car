//
// Created by 57Kindness on 2023/5/25.
//

#include "code_User_Init.h"

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     初始化函数
// 参数说明
// 返回参数
// 使用示例     Init();
// 备注信息
// 作者信息     Created by 57Kindness on 2023/5/25.
//-------------------------------------------------------------------------------------------------------------------
void Init(void) {

    //屏幕初始化,记得放在最前面
    ips200_init(IPS200_TYPE_PARALLEL8);

    //摄像头初始化
    mt9v03x_init();

    //外设初始化
    button_init();

    //陀螺仪初始化
    icm20602_init();

    Left_FlyWheel_Int ();
    Right_FlyWheel_Int ();
    Motor_Control_Int ();

    //无线串口初始化
    wireless_uart_init();

    //闪存初始化在前,清除PID数值
    //PID初始化在前,PID数据任然储存在闪存当中
    PID_Init();
    paraflash_read();
    //PID_Init();

    //中断初始化
    //pit_ms_init(CCU60_CH1, 1);                       //中断通道初始化，此通道用来存放四元数
    pit_ms_init(CCU61_CH0, 1);                       //中断通道初始化，此通道用来存放balance
    pit_ms_init(CCU60_CH0, 100);                     //中断通道初始化，按键消抖

}
