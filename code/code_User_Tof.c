//
// Created by 57Kindness on 2023/6/15.
//

#include "code_User_Tof.h"

bool rxflag = 0, waitflag;
int rxcnt = 0;
int length_val;
int i_2019;
unsigned char rxbuf[16], rxempty;
char ch_2019[16];


void tof_handle(void) {
    if (rxflag) {
        for (i_2019 = 0; i_2019 <= rxcnt; i_2019++) {

            if (ch_2019[i_2019] == 'm') {
                if (ch_2019[i_2019 + 1] == 'm')    //ASCII
                {
                    if ((i_2019 > 0) && (ch_2019[i_2019 - 1] >= '0') && (ch_2019[i_2019 - 1] <= '9'))
                        length_val = ch_2019[i_2019 - 1] - '0';
                    if ((i_2019 > 1) && (ch_2019[i_2019 - 2] >= '0') && (ch_2019[i_2019 - 2] <= '9'))
                        length_val += (ch_2019[i_2019 - 2] - '0') * 10;
                    if ((i_2019 > 2) && (ch_2019[i_2019 - 3] >= '0') && (ch_2019[i_2019 - 3] <= '9'))
                        length_val += (ch_2019[i_2019 - 3] - '0') * 100;
                    if ((i_2019 > 3) && (ch_2019[i_2019 - 4] >= '0') && (ch_2019[i_2019 - 4] <= '9'))
                        length_val += (ch_2019[i_2019 - 4] - '0') * 1000;
                    break;
                }
            }
        }
        rxflag = 0;
        rxcnt = 0;
    } else {
        uart_write_byte(TOF_UART, 'r');
        uart_write_byte(TOF_UART, '6');
        uart_write_byte(TOF_UART, '#');
    }
}


void tof_receive(void) {
    if (!rxflag) {
        if (rxcnt > 15)
            rxcnt = 0;
        ch_2019[rxcnt++] = uart_read_byte(UART_3);
        if (rxcnt > 2) {
            if ((ch_2019[rxcnt - 1] == 'm') && (ch_2019[rxcnt - 2] == 'm'))//
            {
                rxcnt--;
                rxflag = 1;
            }
        }
    }

}

