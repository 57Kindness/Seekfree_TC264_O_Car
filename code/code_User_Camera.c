//
// Created by 57Kindness on 2023/3/1.
//

#include "code_User_Camera.h"

/** 压缩后之后用于存放屏幕显示数据  */
unsigned char Image_Use[LCDH][LCDW];
uint8 image_threshold; //图像阈值
unsigned char Bin_Image[LCDH][LCDW];

unsigned char Line_Count, Last_Line_Count, Start_caculateline, Start_caculate_Flag;
unsigned char Starting_Line_Flag, Starting_Line_Counter; // 起跑线标志位
unsigned char Left_Add_Start, Right_Add_Start;           // 左右补线起始行坐标
unsigned char Left_Add_Stop, Right_Add_Stop;             // 左右补线结束行坐标
unsigned char Out_Side = 0;                              // 丢线控制

unsigned char Straight_Counter;//赛道长度

unsigned char Jump[CountLine + 3];          //跳变点计数
unsigned char Test_Jump;
unsigned char Left_Add_Line[CountLine + 3]; // 左右边界补线数据
unsigned char Right_Add_Line[CountLine + 3];
unsigned char Left_Add_Flag[CountLine + 3];// 左右边界补线标志位
unsigned char Right_Add_Flag[CountLine + 3];
unsigned char Left_Line[CountLine + 3]; // 原始左右边界数据
unsigned char Right_Line[CountLine + 3];
unsigned char Mid_Line[CountLine + 3];
unsigned char Width_Real[CountLine + 3];                                   // 实际赛道宽度
unsigned char Width_Add[CountLine + 3];                                    // 补线赛道宽度
unsigned char Width_Min;    // 最小赛道宽度

/************** 补线赛道相关变量 *************/
float Left_Ka = 0, Right_Ka = 0;
float Left_Kb = 0, Right_Kb = 0;
float Left_Slope, Right_Slope;

/**************特殊列变量*********************/
unsigned char Max_high;
unsigned char Min_high;
///////////////方向控制相关数据///////////////
int16 difference;
int16 servo_duty;
float servo_kp;

////////////速度相关/////////////////
unsigned char foresight;
//////////////////////////////////////
///////////////车库相关数据/////////////
int out_garage;
extern int garage_yaw_init = 0;

///////////////坡道相关数据/////////////
float Jump_pitch;
int Jump_flag;
/////////////////////////////////////////////////////////
//运行标志位
int start_flag; //发车
int run_flag; //运行
//extern unsigned char protect_flag; //保护
////////////////////////////////////////////////////////////
////////////////延时
extern int type_delay;
extern int Start_line_time;
///////////////////////////////////////////////////////////

//圆环变量
unsigned char Left_Ring_flag;
unsigned char Right_Ring_flag;
bool R_Ring_first_White = false;
bool L_Ring_first_White = false;
bool L_Ring_first_Black1 = false;
bool L_Ring_first_Black2 = false;
float ring_ka, ring_kb;
unsigned char Ring_start_line;
unsigned char Ring_stop_line;
unsigned char Left_start_line;
unsigned char Left_stop_line;
float Ring_yaw;
unsigned char Ring_black;
unsigned char ring_num;
unsigned char ring_count = 0;


////p变量
//
//unsigned P_direction;
//
//unsigned char Left_P_flag;
//unsigned char Right_P_flag;
//bool P_first_White=false;
//float P_ka,P_kb;
//unsigned char P_start_line;
//unsigned char P_stop_line;
unsigned char P_flag = 0;
//float p_yaw;


//岔路口
unsigned char left_turn, right_turn;
unsigned char Three_cross_flag;
unsigned char Three_cross_dir;
bool Three_cross_out = false;
unsigned char Weight_Left, Weight_Right;
unsigned char Three_cross_count = 0;
float Three_roads_yaw;

//曲率
uint8 L_angle = 0, R_angle = 0;
bool peak_flag = 0;//判断尖点存在


//foresight

void Get_Use_Image(void) {
    short i = 0, j = 0, row = 0, line = 0;

    for (i = 0; i <= LCDH; i++) {
        for (j = (MT9V03X_W - LCDW) / 2 - 5; j <= (MT9V03X_W + LCDW) / 2 - 5; j++) {
            Image_Use[row][line] = mt9v03x_image[i][j];
            line++;
        }
        line = 0;
        row++;
    }
}


unsigned char GetOSTU(unsigned char tmImage[LCDH][LCDW]) {
    signed short i, j;
    unsigned long Amount = 0;
    unsigned long PixelBack = 0;
    unsigned long PixelshortegralBack = 0;
    unsigned long Pixelshortegral = 0;
    signed long PixelshortegralFore = 0;
    signed long PixelFore = 0;
    float OmegaBack, OmegaFore, MicroBack, MicroFore, SigmaB, Sigma; // 类间方差;
    signed short MinValue, MaxValue;
    signed short Threshold = 0;
    unsigned char HistoGram[256]; //

    for (j = 0; j < 256; j++)
        HistoGram[j] = 0; //初始化灰度直方图

    for (j = 0; j < LCDH; j++) {
        for (i = 0; i < LCDW; i++) {
            HistoGram[tmImage[j][i]]++; //统计灰度级中每个像素在整幅图像中的个数
        }
    }

    for (MinValue = 0; MinValue < 256 && HistoGram[MinValue] == 0; MinValue++); //获取最小灰度的值
    for (MaxValue = 255; MaxValue > MinValue && HistoGram[MinValue] == 0; MaxValue--); //获取最大灰度的值

    if (MaxValue == MinValue)
        return (unsigned char)MaxValue; // 图像中只有一个颜色
    if (MinValue + 1 == MaxValue)
        return (unsigned char)MinValue; // 图像中只有二个颜色

    for (j = MinValue; j <= MaxValue; j++)
        Amount += HistoGram[j]; //  像素总数

    Pixelshortegral = 0;
    for (j = MinValue; j <= MaxValue; j++) {
        Pixelshortegral += HistoGram[j] * j; //灰度值总数
    }
    SigmaB = -1;
    for (j = MinValue; j < MaxValue; j++) {
        PixelBack = PixelBack + HistoGram[j];                                              //前景像素点数
        PixelFore = Amount - PixelBack;                                                    //背景像素点数
        OmegaBack = (float) PixelBack / Amount;                                             //前景像素百分比
        OmegaFore = (float) PixelFore / Amount;                                             //背景像素百分比
        PixelshortegralBack += HistoGram[j] * j;                                           //前景灰度值
        PixelshortegralFore = Pixelshortegral - PixelshortegralBack;                       //背景灰度值
        MicroBack = (float) PixelshortegralBack / PixelBack;                                //前景灰度百分比
        MicroFore = (float) PixelshortegralFore / PixelFore;                                //背景灰度百分比
        Sigma = OmegaBack * OmegaFore * (MicroBack - MicroFore) * (MicroBack - MicroFore); //计算类间方差
        if (Sigma >
            SigmaB)                                                                //遍历最大的类间方差g //找出最大类间方差以及对应的阈值
        {
            SigmaB = Sigma;
            Threshold = j;
        }
    }
    return (unsigned char)Threshold; //返回最佳阈值;
}


char Straight_judge(void) {
    char i;
    char Counter = 0;
    for (i = CountLine; i >= 1; i--) {
        if (Image_Use[i][80] < image_threshold && \
            Image_Use[i][84] < image_threshold && \
            Image_Use[i][76] < image_threshold) {
            break;
        }
    }
    Counter = CountLine - i;
    return Counter; //相当于返回了直道长度，
}


void Get_Bin_Images(uint8 Threshold) {
    unsigned short i = 0, j = 0;
    for (i = 0; i < LCDH; i++) {
        for (j = 0; j < LCDW; j++) {
            if (Image_Use[i][j] > Threshold) //数值越大，显示的内容越多，较浅的图像也能显示出来
                Bin_Image[i][j] = 1;
            else
                Bin_Image[i][j] = 0;
        }
    }
    //Bin_Image_Filter();
}


void Image_Handle(unsigned char *data) {
    unsigned char i;   // 控制行
    unsigned char res; // 用于结果状态判断
    // float Result;   // 用于结果状态判断
    unsigned char Width_Check;
    unsigned char Limit_Left, Limit_Right;


    Line_Count = 0; // 赛道行数复位

    Left_Add_Start = 0; // 复位补线起始，终止行坐标
    Right_Add_Start = 0;
    Left_Add_Stop = 0;
    Right_Add_Stop = 0;
    Start_caculate_Flag = 0;
    Start_caculateline = CountLine;








    /***************************** 最近行特殊处理 *****************************/
    res = First_Line_Handle(data);
    Out_Side = 0;           //丢线控制
    Line_Count = CountLine; //成功识别到的行数就定为59
    /*************************** 第一行特殊处理结束 ***************************/
    for (i = CountLine; i >= 5;) // 仅处理前40行图像，隔行后仅处理20行数据
    {
        i -= 2; // 隔行处理，减小单片机负荷

        if (Left_Add_Flag[i + 2]) //最近行左边就丢线了,//确定本行左右有效宽度
        {
            Limit_Left = Left_Line[i + 2];
        } else {
            if (Jump[i] >= 2) {
                Limit_Left = Left_Add_Line[i + 2];
            } else {
                Limit_Left = Left_Add_Line[i + 2] + 1;
            }
        }

        if (Right_Add_Flag[i + 2]) {
            Limit_Right = Right_Line[i + 2];
        } else {
            if (Jump[i] >= 2) {
                Limit_Right = Right_Add_Line[i + 2];
            } else {
                Limit_Right = Right_Add_Line[i + 2] - 1;
            }
        }
        Jump[i] = Corrode_Filter(i, data, Limit_Left, Limit_Right);

        // 使用腐蚀滤波算法先对本行赛道进行预处理，返回跳变点数量
        if (!Jump_flag && run_flag)//坡道禁止检测起跑线
        {
            if (Jump[i] >= 7 && i >= 50 && Straight_Counter > 40 &&
                ((!Left_Add_Start && Right_Add_Start) ||
                 (Left_Add_Start && !Right_Add_Start))) {
                if (Start_line_time >= 5000) //检测后5s不检测
                {
                    Three_cross_count = Three_cross_count % 2 == 0 ? Three_cross_count : (Three_cross_count + 1) % 4;
                    Start_line_time = 0;
                    Starting_Line_Counter++;
                    ring_count = 0;

                    // rt_mb_send(buzzer_mailbox, 100);
                    Starting_Line_Flag = 1;
                    type_delay = 800;
                }
            }
        }


        if (data[i * 160 + Mid_Line[i + 2]] < image_threshold) //前2行中点在本行为黑点 赛道结束
        {
            break;
        } else // 使用前2行中点向两边扫描边界
        {
            Traversal_Mid_Line(i, data, Mid_Line[i + 2], 1, 159, Left_Line, Right_Line, Left_Add_Line, Right_Add_Line);
        }
        /**************************** 补线检测开始 ****************************/
//        if (Starting_Line_Flag)
//        {
        Width_Check = 5;
//        }
//        else
//        {
//            Width_Check = 5;
//        }
        if (Width_Real[i] > Width_Min + Width_Check) // 赛道宽度变宽，可能是十字或环路
        {

            if (Left_Add_Line[i] < Left_Add_Line[i + 2]) {
                if (!Left_Add_Flag[i]) {
                    Left_Add_Flag[i] = 1; // 强制认定为需要补线
                }
            }
            if (Right_Add_Line[i] > Right_Add_Line[i + 2]) {
                if (!Right_Add_Flag[i]) {
                    Right_Add_Flag[i] = 1; // 强制认定为需要补线
                }
            }

            if (Left_Add_Flag[i] || Right_Add_Flag[i]) {
                if (Left_Add_Stop && Right_Add_Stop) {
                    break;
                }
            }
        }
        /**************************** 补线检测结束 ****************************/

        /*************************** 第一轮补线开始 ***************************/
        if (Left_Add_Flag[i]) // 左侧需要补线
        {
            if (i >= CountLine - 2) // 前三行补线不算
            {
                if (!Left_Add_Start) {
                    Left_Add_Start = i; // 记录补线开始行
                    Left_Ka = 0;
                    Left_Kb = Left_Add_Line[i + 2];
                }
                Left_Add_Line[i] = Calculate_Add(i, Left_Ka, Left_Kb); // 使用前一帧图像左边界斜率补线
            } else {
                if (!Left_Add_Start) // 之前没有补线
                {
                    Left_Add_Start = i; // 记录左侧补线开始行
                    // 斜率补线
                    Curve_Fitting(&Left_Ka, &Left_Kb, &Left_Add_Start, Left_Add_Line, Left_Add_Flag, 1); // 使用两点法拟合直线
                }
                Left_Add_Line[i] = Calculate_Add(i, Left_Ka, Left_Kb); // 补线完成
            }
        } else {
            if (Left_Add_Start) // 已经开始补线
            {
                if (!Left_Add_Stop && !Left_Add_Flag[i + 2]) {
                    if (Left_Add_Line[i] >= Left_Add_Line[i + 2]) {
                        Left_Add_Stop = i; // 记录左侧补线结束行
                    }
                }
            }
        }

        if (Right_Add_Flag[i]) // 右侧需要补线
        {
            if (i >= CountLine - 2) // 前三行补线不算
            {
                if (!Right_Add_Start) {
                    Right_Add_Start = i; // 记录补线开始行
                    Right_Ka = 0;
                    Right_Kb = Right_Add_Line[i + 2];
                }
                Right_Add_Line[i] = Calculate_Add(i, Right_Ka, Right_Kb); // 使用前一帧图像右边界斜率补线
            } else {
                if (!Right_Add_Start) // 之前没有补线
                {
                    Right_Add_Start = i; // 记录右侧补线开始行

                    Curve_Fitting(&Right_Ka, &Right_Kb, &Right_Add_Start, Right_Add_Line, Right_Add_Flag,
                                  2); // 使用两点法拟合直线
                }
                Right_Add_Line[i] = Calculate_Add(i, Right_Ka, Right_Kb); // 补线完成
            }
        } else {
            if (Right_Add_Start) // 已经开始补线
            {
                if (!Right_Add_Stop && !Right_Add_Flag[i + 2]) {
                    if (Right_Add_Line[i] <= Right_Add_Line[i + 2]) {
                        Right_Add_Stop = i; // 记录右侧补线结束行
                    }
                }
            }
        }

        /**********************特殊列判断**************************/
        Min_high = Special_Min_Line(Image_Use[0]);
        Max_high = Special_Max_Line(Image_Use[0]);

        /*************************** 第一轮补线结束 ***************************/
        Width_Add[i] = Right_Add_Line[i] - Left_Add_Line[i];      // 重新计算赛道宽度
        Mid_Line[i] = (Right_Add_Line[i] + Left_Add_Line[i]) / 2; // 计算中线
        if (Width_Add[i] < Width_Min) {
            Width_Min = Width_Add[i]; // 更新最小赛道宽度
        }
        if (Width_Add[i] <= 10) //
        {
            break;
        }
    }
    Line_Count = i; // 记录成功识别到的赛道行数

    /*************************** 第二轮补线修复开始 ***************************/
    if (Left_Add_Start) // 左边界需要补线
    {
        Line_Repair(Left_Add_Start, Left_Add_Stop, data, Left_Line, Left_Add_Line, Left_Add_Flag, 1); //直接使用两点斜率进行补线
    }
    if (Right_Add_Start) // 右边界需要补线
    {
        Line_Repair(Right_Add_Start, Right_Add_Stop, data, Right_Line, Right_Add_Line, Right_Add_Flag,
                    2); //直接使用两点斜率进行补线
    }
    /*************************** 第二轮补线修复结束 ***************************/

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////检测特殊元素
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    //拐点计算
    //peak_flag = Turn_calculate(L_angle, R_angle);
    peak_flag = Turn_calculate();
    //wofa_data[0]=peak_flag;

    if ((!R_Ring_first_White || !L_Ring_first_White) && !Right_Ring_flag && !Left_Ring_flag) {
        Ring_yaw = Yaw1;
    }

    //圆环检测
    if (type_delay <= 0 && !Jump_flag)//count_Ring_judge3<0&&
    {
        Ring_judge();
    }
    if (ring_count > ring_num) {
        Right_Ring_flag = 0;
        Left_Ring_flag = 0;
    }
    //圆环补线
    if (Left_Ring_flag || Right_Ring_flag) {
        Ring_process();

    }

    if (!Three_cross_flag) {
        Three_roads_yaw = Yaw1;
    }
    //三叉检测

//    if(Starting_Line_Counter<STOP_LINE&&
//       type_delay<=0&&run_flag&&
//       !Left_Ring_flag&&!Right_Ring_flag&&
//       !Jump_flag)
//    Three_roads_judge();
//    //三叉补线
//    if(Three_cross_flag)
//    Three_roads_process();
//
//
//    //p环检测
//    if(!Right_P_flag && !Left_P_flag)
//    {
//        p_yaw = yaw;
//    }
//    if( Starting_Line_Counter<STOP_LINE&&
//        !Three_cross_out&&
//        type_delay<=0&&
//        run_flag&&
//        !Left_Ring_flag&&
//        !Right_Ring_flag&&
//        P_Delay<0&&
//        !Jump_flag)
//    {
//        P_judge();
//    }
//    //p环补线
//    if(
//       !Three_cross_out&&
//       (Right_P_flag||Left_P_flag)&&
//        !Left_Ring_flag&&
//        !Right_Ring_flag&&
//        P_Delay<0&&
//        !Jump_flag)
//    {
//        P_process();
//    }

//    //入库判断
//    if(Starting_Line_Counter<STOP_LINE&&out_garage)
//            garage_yaw = yaw;




    /****************************** 中线修复开始 ******************************/
    Mid_Line_Repair(Line_Count, data); //左右边线补线修复后 对中线重新赋值
    /****************************** 中线修复结束 ******************************/

}



////三叉处理
//void Three_roads_process()
//{

//        float three_ka,three_kb;
//        unsigned char i ;
//        if(Three_cross_dir==Three_road_l)
//        {
//            if(right_turn)
//            {
//               i = right_turn;
//               Line_Count=CountLine-Straight_Counter;
//               Slope_calculation(&three_ka,&three_kb,i,Right_Line[i],CountLine-Straight_Counter,40,0);
//               for(i-=2;i>CountLine-Straight_Counter;i-=2)
//               {
//                   Right_Add_Line[i]=Calculate_Add(i,three_ka,three_kb);
//                   Left_Add_Line[i]=10;
//               }
//            }
//            else
//            {
//               i = CountLine;
//               Line_Count=30;
//               Slope_calculation(&three_ka,&three_kb,i,159,Line_Count,40,0);
//               for(i-=2;i>=Line_Count;i-=2)
//               {
//                   Right_Add_Line[i]=Calculate_Add(i,three_ka,three_kb);
//                   Left_Add_Line[i]=10;
//               }
//            }
//            if(angle_diff(yaw, Three_roads_yaw)>=15)
//            {
//                Three_cross_flag=0;
//                type_delay=TYPE_DELAY;
//    //            Three_cross_out=false;
//            }
//        }
//
//        if(Three_cross_dir==Three_road_r)
//        {
//            if(left_turn)
//            {
//               i = left_turn;
//               Line_Count=CountLine-Straight_Counter;
//               Slope_calculation(&three_ka,&three_kb,i,Left_Line[i],CountLine-Straight_Counter,120,1);
//               for(i-=2;i>CountLine-Straight_Counter;i-=2)
//               {
//                   Left_Add_Line[i]=Calculate_Add(i,three_ka,three_kb);
//                   Right_Add_Line[i]=150;
//               }
//            }
//            else
//            {
//               i = CountLine;
//               Line_Count=30;
//               Slope_calculation(&three_ka,&three_kb,i,1,Line_Count,120,1);
//               for(i-=2;i>=Line_Count;i-=2)
//               {
//                   Left_Add_Line[i]=Calculate_Add(i,three_ka,three_kb);
//                   Right_Add_Line[i]=150;
//               }
//            }
//
//        }


//    if(angle_diff(yaw, Three_roads_yaw)<=-25||angle_diff(eulerAngle.yaw, Three_roads_yaw)>=25)
//    {
//        Three_cross_flag=0;
//        type_delay=TYPE_DELAY;
//    }
//}
//void Three_roads_judge()
//{
//
//    if(!Three_cross_out&&(Three_cross_count==0||Three_cross_count==2))
//    {
//
//        if(
//            R_angle>=100&&//R_angle<=160&&
//            L_angle>=100&&//L_angle<=160&&
//           Straight_Counter<=80&&//
//           !Three_cross_flag&&
//           //Three_cross_Delay<=0&&
//           peak_flag
//           )
//
//        {
//            Three_cross_out=true;
//            //Three_cross_Delay=500;
//            type_delay=TYPE_DELAY;
//            rt_mb_send(buzzer_mailbox, 100);
//            Three_cross_flag=1;
//            Three_cross_count++;
//            if(sw2_status)
//            {
//                Three_cross_dir=Three_road_r;
//            }
//            else
//            {
//                Three_cross_dir=Three_road_l;
//            }
//        }
//    }
//    else
//    {
//        if(
////           Left_Add_Start<=85&&Right_Add_Start<=85&&
//           Left_Add_Start>=65&&Right_Add_Start>=65&&
////           Straight_Counter<=70&&//
//           !Three_cross_flag//&&
//           //Three_cross_Delay<=0&&
//           //peak_flag
//           )
//
//        {
//            Three_cross_out=false;
//            type_delay=TYPE_DELAY;
//            rt_mb_send(buzzer_mailbox, 100);
//            Three_cross_flag=1;
//            Three_cross_count++;
//            if(sw2_status)
//            {
//                Three_cross_dir=Three_road_r;
//            }
//            else
//            {
//                Three_cross_dir=Three_road_l;
//            }
//        }
//    }
//    if(Three_cross_count==4)
//    {
//        Three_cross_count=0;
//    }
//
////    wofa_data[0] = kl;
////    wofa_data[1] = kr;
//}
//}


//拐点计算及尖点判断
//拐点计算及尖点判断
bool Turn_calculate(void) {
    unsigned char i;
//    unsigned char L_count=0,R_count=0;
    Point A, B, C;
    unsigned char Left_Max = 0, Right_Min = 160;//用于拐点计算
//    unsigned char mid_left,mid_right;
//    uint8 peak_point=0;
    unsigned char l_row = 0, r_row = 0, m_row = 0;
    unsigned char Mid_Point;
    right_turn = 0;
    left_turn = 0;
    L_angle = 0;
    R_angle = 0;
    P_flag = 0;

    if (Left_Add_Start >= 80) {
        left_turn = 81;
    } else if (Left_Add_Start > 30) {
        for (i = CountLine; i >= Left_Add_Start; i -= 2) {
            if (Left_Max <= Left_Line[i]) {
                Left_Max = Left_Line[i];
                left_turn = i;
            }
        }
    }

    if (Right_Add_Start >= 80) {
        right_turn = 81;
    } else if (Right_Add_Start > 30) {
        for (i = CountLine; i >= Right_Add_Start; i -= 2) {
            if (Right_Min >= Right_Line[i]) {
                Right_Min = Right_Line[i];
                right_turn = i;
            }
        }
    }

    if (left_turn >= 30 &&
        left_turn <= 80 &&
        right_turn >= 30 &&
        right_turn <= 80) {
        A.m_i8x = left_turn - 4;
        A.m_i8y = Left_Add_Line[A.m_i8x];
        B.m_i8x = left_turn;
        B.m_i8y = Left_Add_Line[B.m_i8x];
        C.m_i8x = left_turn + 4;
        C.m_i8y = Left_Add_Line[C.m_i8x];
        L_angle = Get_Angle(A, B, C);
        A.m_i8x = right_turn - 4;
        A.m_i8y = Right_Add_Line[A.m_i8x];
        B.m_i8x = right_turn;
        B.m_i8y = Right_Add_Line[B.m_i8x];
        C.m_i8x = right_turn + 4;
        C.m_i8y = Right_Add_Line[C.m_i8x];
        R_angle = Get_Angle(A, B, C);

//        wofa_data[0]=L_angle;
//        wofa_data[1]=R_angle;
    }

    for (i = left_turn; i >= 10; i -= 2) {
        if (Image_Use[i][Left_Add_Line[left_turn] + 5] < image_threshold) {
            l_row = i;
            break;
        }
    }
    for (i = right_turn; i >= 10; i -= 2) {
        if (Image_Use[i][Right_Add_Line[right_turn] - 5] < image_threshold) {
            r_row = i;
            break;
        }
    }
    Mid_Point = (Left_Add_Line[left_turn] + Right_Add_Line[right_turn]) / 2;
    for (i = (right_turn + left_turn) / 2; i >= 10; i -= 2) {
        if (Image_Use[i][Mid_Point] < image_threshold) {
            m_row = i;
            break;
        }
    }
//    wofa_data[0]=l_row;
//    wofa_data[1]=r_row;
//    wofa_data[2]=m_row;
    if (m_row != 0 && l_row != 0 && r_row != 0) {
        if ((l_row <= m_row && m_row <= r_row) || (l_row >= m_row && m_row >= r_row)) {
            P_flag = 1;
        }
        if (l_row < m_row && r_row < m_row) {
            return 1;
        }
    }
    return 0;
}


uint8 Get_Angle(Point A, Point B, Point C) {
    Point Temp_Point[3]; //储存透视变换之后的点
    Point Vector[2];     //储存向量
    float x = 0;
    uint8 a = 0;
    Temp_Point[0] = A; //get_inv_img(A);
    Temp_Point[1] = B; //get_inv_img(B);
    Temp_Point[2] = C; //get_inv_img(C);
    Vector[0].m_i8x = (Temp_Point[0].m_i8x - Temp_Point[1].m_i8x);
    Vector[0].m_i8y = (Temp_Point[0].m_i8y - Temp_Point[1].m_i8y);
    Vector[1].m_i8x = (Temp_Point[2].m_i8x - Temp_Point[1].m_i8x);
    Vector[1].m_i8y = (Temp_Point[2].m_i8y - Temp_Point[1].m_i8y);
    x = (float) ((double) (Vector[0].m_i8x * Vector[1].m_i8x + Vector[0].m_i8y * Vector[1].m_i8y) / (double) (OSG_Sqrt(
            (float)(Vector[0].m_i8x * Vector[0].m_i8x + Vector[0].m_i8y * Vector[0].m_i8y) *
            (Vector[1].m_i8x * Vector[1].m_i8x + Vector[1].m_i8y * Vector[1].m_i8y))));
    a = (uint8)((acos(x) * 180) / MATH_PI);
    return (a);
}

/*开平方函数*/
float OSG_Sqrt(float x) {
    float xhalf = 0.5f * x;
    int i = *(int *) &x; // get bits for floating VALUE
    i = 0x5f375a86 - (i >> 1); // gives initial guess y0
    x = *(float *) &i; // convert bits BACK to float
    x = x * (1.5f - xhalf * x * x); // Newton step, repeating increases accuracy
    x = x * (1.5f - xhalf * x * x); // Newton step, repeating increases accuracy
    x = x * (1.5f - xhalf * x * x); // Newton step, repeating increases accuracy
    return ((float) (1.0 / x));
}
//P环检测
//void P_judge(void)
//{
//
//    if(!Left_Add_Start&&Right_Add_Start)
//    {
//        P_direction=P_r;
//    }
//    else if(Left_Add_Start&&!Right_Add_Start)
//    {
//        P_direction=P_l;
//    }
//
//    if(P_flag&&
//       Left_Add_Start>65&&
//       Right_Add_Start>65&&
//       Straight_Counter<=60&&
////       Straight_Counter>=40&&
//       Min_high>30&&//Min_high<60&&
//       Max_high>30&&//Max_high<60&&
//       abs(Min_high-Max_high)<=15
//       )
//    {
//        switch(P_direction)
//        {
//            case P_r:
//
//                {
//                    Right_P_flag = 1;
//                    Left_P_flag = 0;
//                }
//                break;
//            case P_l:
//                {
//                    Left_P_flag = 1;
//                    Right_P_flag = 0;
//                }
//                break;
//        }
//    }
//}
//
//
//void P_process(void)
//{
//    unsigned char i,j;
//    if(Right_P_flag)
//    {
//        if(angle_diff(yaw, p_yaw)<45)
//        {
//            rt_mb_send(buzzer_mailbox, 50);
//            Slope_calculation(&P_ka,&P_kb,CountLine,Right_Add_Line[CountLine],Line_Count,1,0);
//            for(i=CountLine-2;i>Line_Count;i--)
//             {
//                 Right_Add_Line[i]=Calculate_Add(i,P_ka,P_kb);
//                 Left_Add_Line[i]=1;
//             }
//        }
//        else
//        {
//            Right_P_flag = 0;
//        }
//    }
//
//    if(Left_P_flag)
//    {
//        if(angle_diff(yaw, p_yaw)>-45)
//        {
//            rt_mb_send(buzzer_mailbox, 50);
//            Slope_calculation(&P_ka,&P_kb,CountLine,Left_Add_Line[CountLine],Line_Count,159,1);
//            for(j=CountLine-2;j>Line_Count;j--)
//             {
//                 Left_Add_Line[j]=Calculate_Add(j,P_ka,P_kb);
//                 Right_Add_Line[j]=159;
//             }
//        }
//        else
//        {
//            Left_P_flag = 0;
//        }
//    }
//
//
//}

//圆环检测
void Ring_judge(void) {

    bool left_single;
    bool right_single;
    //右环检测
    if (Right_Add_Start >= (CountLine - 3) &&
        // !Left_Add_Start&&//
        (Right_Add_Start - Right_Add_Stop) <= 25 &&
        //(Right_Add_Start-Right_Add_Stop)>=5&&
        Straight_Counter >= 89 &&             //(LCDH-10)&&
        Right_Ring_flag == 0
            ) {
        R_Ring_first_White = 1;
    }

    if (angle_diff(Yaw1, Ring_yaw) > 25 && R_Ring_first_White) {
        R_Ring_first_White = 0;
    }


    if (R_Ring_first_White) {
        left_single = Slope_Left_Line(Left_Line, Line_Count + 4, 1); //确定左边界为单调
        if (!left_single) {
            R_Ring_first_White = 0;
        }
    }

    //检测第二个白的区域
    if (R_Ring_first_White == 1 &&
        Right_Add_Start < 60 &&
        // Right_Add_Start > 30&&
        //!Left_Add_Start&&
        (Right_Add_Start - Right_Add_Stop) >= 15 &&
        Straight_Counter >= 89 &&//(LCDH-10)&&
        Right_Ring_flag == 0) {
//             P_Delay=2000;
        R_Ring_first_White = 0;
        Right_Ring_flag = 1;
        //ring_count++;
        //rt_mb_send(buzzer_mailbox, 50);

    }


    //左环检测
    if (Left_Add_Start >= (CountLine - 3) &&
        //!Right_Add_Start&&
        (Left_Add_Start - Left_Add_Stop) <= 25 &&
        //(Left_Add_Start-Left_Add_Stop)>=5&&
        Straight_Counter >= (LCDH - 1) &&
        Left_Ring_flag == 0
            ) {
        L_Ring_first_White = 1;
    }


    if (angle_diff(Yaw1, Ring_yaw) < -25 && L_Ring_first_White) {
        L_Ring_first_White = 0;
    }


    if (L_Ring_first_White) {
        right_single = Slope_Right_Line(Right_Line, Line_Count, 1); //确定右边界为单调
        if (!right_single) {
            L_Ring_first_White = 0;
            L_Ring_first_Black1 = 0;
            L_Ring_first_Black2 = 0;
        }
    }


    if (L_Ring_first_White == 1) {
        if (Image_Use[89][1] > image_threshold) {
            L_Ring_first_Black1 = 1;

        }
        //else
        //L_Ring_first_Black1=0;
    }

    if (L_Ring_first_Black1 == 1 && L_Ring_first_White == 1) {
        if (Image_Use[89][1] < image_threshold) {
            L_Ring_first_Black2 = 1;
        }
        //else
        //L_Ring_first_Black2=0;
    }


    if (L_Ring_first_White) {
        right_single = Slope_Right_Line(Right_Line, Line_Count + 4, Ring); //确定右边界为单调
        if (!right_single) {
            L_Ring_first_White = 0;
            L_Ring_first_Black1 = 0;
            L_Ring_first_Black2 = 0;
        }
    }




    //检测第二个白的区域
    if (L_Ring_first_White == 1 &&
        Left_Add_Start < (60) &&
        Left_Add_Start > (30) &&
        !Right_Add_Start &&
        (Left_Add_Start - Left_Add_Stop) >= 15 &&
        Straight_Counter >= (LCDH - 10) &&
        Left_Ring_flag == 0) {
//             P_Delay=2000;
        L_Ring_first_White = 0;
        Left_Ring_flag = 1;
        ring_count++;
        // rt_mb_send(buzzer_mailbox, 50);
    }

}

//圆环处理
void Ring_process(void) {
    unsigned char i = Right_Add_Start;
    unsigned char j = Left_Add_Start;
    unsigned char k;
    unsigned char out_ring_stop = 40;

    Three_cross_count = Three_cross_count % 2 == 0 ? Three_cross_count : (Three_cross_count + 1) % 4;
    //右环处理
    if (Right_Ring_flag == 1) {

        Ring_start_line = i;
        Ring_stop_line = 15;
        Slope_calculation(&ring_ka, &ring_kb, Ring_start_line, Left_Line[i], Ring_stop_line, 159, 1);
        for (i -= 2; i > Ring_stop_line; i -= 2) {
            Left_Add_Line[i] = Calculate_Add(i, ring_ka, ring_kb);
            Right_Add_Line[i] = 159;
        }
        if (Straight_Counter <= 50) {
//            P_Delay=2000;
            Right_Ring_flag = 2;
        }
    }


    if (Right_Ring_flag == 2) {
        if (Right_Add_Start > 85) {
            for (unsigned char u = CountLine; u > Line_Count; u -= 2) {
                Right_Add_Line[u] = 159;
            }
        }
        if (Left_Add_Start > 67 && angle_diff(Yaw1, Ring_yaw) > 60) {
//            P_Delay=5000;
            Right_Ring_flag = 3;
        }

    }


    if (Right_Ring_flag == 3) {
        k = Left_Add_Start;
        Slope_calculation(&ring_ka, &ring_kb, Left_Add_Start, Left_Add_Line[Left_Add_Start], out_ring_stop, 159, 1);
        for (k -= 2; k > out_ring_stop; k -= 2) {
            Left_Add_Line[k] = Calculate_Add(k, ring_ka, ring_kb);
            Right_Add_Line[k] = 159;

        }

        if (Left_Add_Stop >= 60 || Straight_Counter > LCDH - 15) {
            // P_Delay=2000;
            //type_delay=TYPE_DELAY;
            Right_Ring_flag = 0;
            //rt_mb_send(buzzer_mailbox, 50);
        }
    }



    //左环处理
    if (Left_Ring_flag == 1) {
        Ring_start_line = j;
        Ring_stop_line = 15;
        Slope_calculation(&ring_ka, &ring_kb, Ring_start_line, Right_Line[j], Ring_stop_line, 1, 0);
        for (j -= 2; j > Ring_stop_line; j -= 2) {
            Right_Add_Line[j] = Calculate_Add(j, ring_ka, ring_kb);
            Left_Add_Line[j] = 10;
        }
        if (Straight_Counter <= 50) {
//            P_Delay=5000;
            Left_Ring_flag = 2;
        }
    }


    if (Left_Ring_flag == 2) {
        if (Left_Add_Start > 85) {
            for (unsigned char u = CountLine; u > Line_Count; u -= 2) {
                Left_Add_Line[u] = 1;
            }
        }
        if (Right_Add_Start > 65 && angle_diff(Yaw1, Ring_yaw) < -60) {
//            P_Delay=2000;
            Left_Ring_flag = 3;
        }

    }


    if (Left_Ring_flag == 3) {
        k = Right_Add_Start;
        Slope_calculation(&ring_ka, &ring_kb, Right_Add_Start, Right_Add_Line[Right_Add_Start], out_ring_stop, 1, 0);
        for (k -= 2; k > out_ring_stop; k -= 2) {
            Right_Add_Line[k] = Calculate_Add(k, ring_ka, ring_kb);
            Left_Add_Line[k] = 10;
        }

        if (Right_Add_Stop >= 60 || Straight_Counter > LCDH - 15) {
            //P_Delay=2000;
            //count_Ring_judge3=2000;
            Left_Ring_flag = 0;
            //type_delay=TYPE_DELAY;
            //rt_mb_send(buzzer_mailbox, 50);
        }
    }


}

//mode为1是右环,0为左环
void Slope_calculation(float *ka, float *kb, unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2,
                       unsigned char mode) {
    *ka = 1.0 * (y1 - y2) / (x1 - x2);
    if (mode) {
        if (*ka > 0)
            *ka = 0;
    } else {
        if (*ka < 0)
            *ka = 0;
    }
    *kb = 1.0 * y1 - (*ka * x1);
}


unsigned char First_Line_Handle(unsigned char *data) //精妙！！！
{
    unsigned char i;                         // 控制行
    unsigned char Weight_Left, Weight_Right; // 左右赛道宽度
    unsigned char Mid_Left, Mid_Right;
    unsigned char res;
    i = CountLine;
    res = Corrode_Filter(i, data, 1, 159); // 使用腐蚀滤波算法先对本行赛道进行预处理，返回跳变点数量
    Jump[CountLine] = res;
    //赛道保护
    // if (data[i * 160 +1] < image_threshold && data[i * 160 + 159]< image_threshold && data[i * 160 +80] < image_threshold&&start_flag)
    //protect_flag = 0;
    if (data[i * 160 + Mid_Line[CountLine + 2]] < image_threshold) // 最近行中点在第倒数第二行为黑点 （急弯处理）
    {
        Weight_Left = Traversal_Left(i, data, &Mid_Left, 1, 159);    // 从左侧搜索边界
        Weight_Right = Traversal_Right(i, data, &Mid_Right, 1, 159); // 从右侧搜索边界
        if (Weight_Left >= Weight_Right && Weight_Left)              // 左赛道宽度大于右赛道宽度且不为0
        {
            Traversal_Left_Line(i, data, Left_Line, Right_Line); // 使用左遍历获取赛道边界
        } else if (Weight_Left < Weight_Right && Weight_Right) {
            Traversal_Right_Line(i, data, Left_Line, Right_Line); // 使用右遍历获取赛道边界
        } else // 说明没查到
        {
            return 0;
        }
    } else {
        Traversal_Mid_Line(i, data, Mid_Line[i + 2], 1, 159, Left_Line, Right_Line, Left_Add_Line,
                           Right_Add_Line); // 从前一行中点向两边扫描
    }
    Left_Line[CountLine + 2] = Left_Line[CountLine];
    Right_Line[CountLine + 2] = Right_Line[CountLine];
    Left_Add_Line[CountLine + 2] = Left_Add_Line[CountLine];
    Right_Add_Line[CountLine + 2] = Right_Add_Line[CountLine];
    if (Left_Add_Flag[CountLine] && Right_Add_Flag[CountLine]) {
        Mid_Line[CountLine] = Mid_Line[CountLine + 2];
    } else {
        Mid_Line[CountLine] = (Right_Line[CountLine] + Left_Line[CountLine]) / 2;
        Mid_Line[CountLine + 2] = Mid_Line[CountLine]; // 更新第60行虚拟中点，便于下一帧图像使用
    }
    if (Left_Add_Flag[CountLine]) {
        Left_Add_Start = i; // 记录补线开始行
        Left_Ka = 0;
        Left_Kb = Left_Add_Line[CountLine]; //斜率补线的起点横坐标，纵坐标都记录下来了
    }
    if (Right_Add_Flag[CountLine]) {
        Right_Add_Start = i; // 记录补线开始行
        Right_Ka = 0;
        Right_Kb = Right_Add_Line[CountLine]; //斜率补线的起点横坐标，纵坐标都记录下来了
    }
    Width_Real[CountLine + 2] = Width_Real[CountLine];
    Width_Add[CountLine + 2] = Width_Add[CountLine];
    Width_Min = Width_Add[CountLine];

    return 1;
}

/*
 *
 *
 *特殊列处理
 */
unsigned char Special_Min_Line(unsigned char *data) {
    unsigned char i;
    unsigned char white_flag = 0;
    unsigned char Min_low_line, Min_on_line;


    for (i = CountLine; i >= 1; i--) {
        if (!white_flag) {
            if (data[i * 160 + MIN_LINE] >= image_threshold) {
                Min_low_line = i;
                white_flag = 1;
                continue;
            }
        }
        if (white_flag) {
            if (data[i * 160 + MIN_LINE] <= image_threshold) {
                Min_on_line = i - 1;
                break;
            }
        }

    }


    if (!white_flag) {
        return 0;
    } else {
        return (Min_low_line - Min_on_line);
    }


}

unsigned char Special_Max_Line(unsigned char *data) {
    unsigned char i;
    unsigned char white_flag = 0;
    unsigned char Max_low_line, Max_on_line;

    for (i = CountLine; i >= 1; i--) {
        if (!white_flag) {
            if (data[i * 160 + MAX_LINE] >= image_threshold) {
                Max_low_line = i;
                white_flag = 1;
                continue;
            }
        }
        if (white_flag) {
            if (data[i * 160 + MAX_LINE] <= image_threshold) {
                Max_on_line = i - 1;
                break;
            }
        }


    }


    if (!white_flag) {
        return 0;
    } else {
        return (Max_low_line - Max_on_line);
    }


}

/*
左边界斜率计算
*
*/
unsigned char Slope_Left_Line(unsigned char *Left, unsigned char stop, enum TYPE type) {
    char Slope;
//    char Line_start=CountLine;
    switch (type) {
        case 1:
            if (Left[stop]) {
                Slope = (Left[CountLine] - Left[stop]);//
                //wofa_data[1]=Slope;
                if (Slope >= -55)//&&Slope <= -25)
                    return 1;
            }
            return 0;
    }
    return 0;
}


//右边界斜率计算
unsigned char Slope_Right_Line(unsigned char *Right, unsigned char stop, enum TYPE type) {
    float Slope;
    switch (type) {
        case 1:
            if (Right[stop]) {
                Slope = (Right[CountLine] - Right[stop]);//5行计算一次斜率
                //wofa_data[0]=Slope;
                if (Slope <= 65)//&&Slope >= 25)
                    return 1;
            }
            return 0;

    }
    return 0;

}


unsigned char Corrode_Filter(unsigned char i, unsigned char *data, unsigned char Left_Min, unsigned char Right_Max) {
    unsigned char j;
    unsigned char White_Flag = 0;
    unsigned char Jump_Count = 0; // 跳变点计数
    Test_Jump = 0;

    Right_Max = (unsigned char) func_limit_ab(Right_Max, 1, 159); // 保留右侧部分区域，防止溢出

    for (j = Left_Min; j <= Right_Max; j++) // 从左向右扫描，方向不影响结果
    {
        if (!White_Flag) // 先查找白点，只滤黑点，不滤白点
        {
            if (data[i * 160 + j] >= image_threshold) // 检测到白点
            {
                White_Flag = 1; // 开始找黑点
            }
        } else {
            if (data[i * 160 + j] < image_threshold) // 检测到黑点
            {
                Jump_Count++; // 视为一次跳变

                Test_Jump = Jump_Count;

                if (data[i * 160 + j] < image_threshold && j + 1 <= Right_Max) // 连续两个黑点
                {
                    if (data[i * 160 + j + 2] < image_threshold && j + 2 <= Right_Max) // 连续三个黑点
                    {
                        if (data[i * 160 + j + 3] < image_threshold && j + 3 <= Right_Max) // 连续四个黑点
                        {
                            if (data[i * 160 + j + 4] < image_threshold && j + 4 <= Right_Max) // 连续五个黑点
                            {
                                if (data[i * 160 + j + 5] < image_threshold && j + 5 <= Right_Max) // 连续六个黑点
                                {
                                    if (data[i * 160 + j + 6] < image_threshold && j + 6 <= Right_Max) // 连续七个黑点
                                    {
                                        if (data[i * 160 + j + 7] < image_threshold && j + 7 <= Right_Max) // 连续八个黑点
                                        {
                                            if (data[i * 160 + j + 8] < image_threshold && j + 8 <= Right_Max) // 连续九个黑点
                                            {
                                                if (data[i * 160 + j + 9] < image_threshold &&
                                                    j + 9 <= Right_Max) // 连续十个黑点
                                                {
                                                    if (data[i * 160 + j + 10] < image_threshold &&
                                                        j + 10 <= Right_Max) // 连续11个黑点
                                                    {
                                                        White_Flag = 0; // 认为不是干扰，不做任何处理，下次搜索白点
                                                        j += 10;
                                                    } else if (j + 10 <= Right_Max) {
                                                        data[i * 160 + j] = 255;     // 仅有连续10个黑点，滤除掉，（黑点变白点，防干扰）
                                                        data[i * 160 + j + 1] = 255; // 仅有连续10个黑点，滤除掉
                                                        data[i * 160 + j + 2] = 255; // 仅有连续10个黑点，滤除掉
                                                        data[i * 160 + j + 3] = 255; // 仅有连续10个黑点，滤除掉
                                                        data[i * 160 + j + 4] = 255; // 仅有连续10个黑点，滤除掉
                                                        data[i * 160 + j + 5] = 255; // 仅有连续10个黑点，滤除掉
                                                        data[i * 160 + j + 6] = 255; // 仅有连续10个黑点，滤除掉
                                                        data[i * 160 + j + 7] = 255; // 仅有连续10个黑点，滤除掉
                                                        data[i * 160 + j + 8] = 255; // 仅有连续10个黑点，滤除掉
                                                        data[i * 160 + j + 9] = 255; // 仅有连续10个黑点，滤除掉

                                                        j += 10;
                                                    } else {
                                                        j += 10;
                                                    }
                                                } else if (j + 9 <= Right_Max) {
                                                    data[i * 160 + j] = 255;     // 仅有连续九个黑点，滤除掉
                                                    data[i * 160 + j + 1] = 255; // 仅有连续九个黑点，滤除掉
                                                    data[i * 160 + j + 2] = 255; // 仅有连续九个黑点，滤除掉
                                                    data[i * 160 + j + 3] = 255; // 仅有连续九个黑点，滤除掉
                                                    data[i * 160 + j + 4] = 255; // 仅有连续九个黑点，滤除掉
                                                    data[i * 160 + j + 5] = 255; // 仅有连续九个黑点，滤除掉
                                                    data[i * 160 + j + 6] = 255; // 仅有连续九个黑点，滤除掉
                                                    data[i * 160 + j + 7] = 255; // 仅有连续九个黑点，滤除掉
                                                    data[i * 160 + j + 8] = 255; // 仅有连续九个黑点，滤除掉

                                                    j += 9;
                                                } else {
                                                    j += 9;
                                                }
                                            } else if (j + 8 <= Right_Max) {
                                                data[i * 160 + j] = 255;     // 仅有连续八个黑点，滤除掉
                                                data[i * 160 + j + 1] = 255; // 仅有连续八个黑点，滤除掉
                                                data[i * 160 + j + 2] = 255; // 仅有连续八个黑点，滤除掉
                                                data[i * 160 + j + 3] = 255; // 仅有连续八个黑点，滤除掉
                                                data[i * 160 + j + 4] = 255; // 仅有连续八个黑点，滤除掉
                                                data[i * 160 + j + 5] = 255; // 仅有连续八个黑点，滤除掉
                                                data[i * 160 + j + 6] = 255; // 仅有连续八个黑点，滤除掉
                                                data[i * 160 + j + 7] = 255; // 仅有连续八个黑点，滤除掉

                                                j += 8;
                                            } else {
                                                j += 8;
                                            }
                                        } else if (j + 7 <= Right_Max) {
                                            data[i * 160 + j] = 255;     // 仅有连续七个黑点，滤除掉
                                            data[i * 160 + j + 1] = 255; // 仅有连续七个黑点，滤除掉
                                            data[i * 160 + j + 2] = 255; // 仅有连续七个黑点，滤除掉
                                            data[i * 160 + j + 3] = 255; // 仅有连续七个黑点，滤除掉
                                            data[i * 160 + j + 4] = 255; // 仅有连续七个黑点，滤除掉
                                            data[i * 160 + j + 5] = 255; // 仅有连续七个黑点，滤除掉
                                            data[i * 160 + j + 6] = 255; // 仅有连续七个黑点，滤除掉

                                            j += 7;
                                        } else {
                                            j += 7;
                                        }
                                    } else if (j + 6 <= Right_Max) {
                                        data[i * 160 + j] = 255;     // 仅有连续六个黑点，滤除掉
                                        data[i * 160 + j + 1] = 255; // 仅有连续六个黑点，滤除掉
                                        data[i * 160 + j + 2] = 255; // 仅有连续六个黑点，滤除掉
                                        data[i * 160 + j + 3] = 255; // 仅有连续六个黑点，滤除掉
                                        data[i * 160 + j + 4] = 255; // 仅有连续六个黑点，滤除掉
                                        data[i * 160 + j + 5] = 255; // 仅有连续六个黑点，滤除掉

                                        j += 6;
                                    } else {
                                        j += 6;
                                    }
                                } else if (j + 5 <= Right_Max) {
                                    data[i * 160 + j] = 255;     // 仅有连续五个黑点，滤除掉
                                    data[i * 160 + j + 1] = 255; // 仅有连续五个黑点，滤除掉
                                    data[i * 160 + j + 2] = 255; // 仅有连续五个黑点，滤除掉
                                    data[i * 160 + j + 3] = 255; // 仅有连续五个黑点，滤除掉
                                    data[i * 160 + j + 4] = 255; // 仅有连续五个黑点，滤除掉

                                    j += 5;
                                } else {
                                    j += 5;
                                }
                            } else if (j + 4 <= Right_Max) {
                                data[i * 160 + j] = 255;     // 仅有连续四个黑点，滤除掉
                                data[i * 160 + j + 1] = 255; // 仅有连续四个黑点，滤除掉
                                data[i * 160 + j + 2] = 255; // 仅有连续四个黑点，滤除掉
                                data[i * 160 + j + 3] = 255; // 仅有连续四个黑点，滤除掉

                                j += 4;
                            } else {
                                j += 4;
                            }
                        } else if (j + 3 <= Right_Max) {
                            data[i * 160 + j] = 255;     // 仅有连续三个黑点，滤除掉
                            data[i * 160 + j + 1] = 255; // 仅有连续三个黑点，滤除掉
                            data[i * 160 + j + 2] = 255; // 仅有连续三个黑点，滤除掉

                            j += 3;
                        } else {
                            j += 3;
                        }
                    } else if (j + 2 <= Right_Max) {
                        data[i * 160 + j] = 255;     // 仅有连续两个黑点，滤除掉
                        data[i * 160 + j + 1] = 255; // 仅有连续两个黑点，滤除掉

                        j += 2;
                    } else {
                        j += 2;
                    }
                } else if (j + 1 <= Right_Max) {
                    data[i * 160 + j] = 255; // 有一个黑点，滤除掉

                    j += 1;
                } else {
                    j += 1;
                }
            }
        }
    }
    //  if (White_Flag)
    //  {
    //      Jump_Count++;   // 视为一次跳变
    //  }

    return Jump_Count; // 返回跳变点计数
}


/*
从左侧搜索黑边界
*/
unsigned char
Traversal_Black_Left(unsigned char i, unsigned char *data, unsigned char Left_Min, unsigned char Right_Max) {
    unsigned char j;
    unsigned char Black_Leftline;
//    unsigned char Left_Line = Left_Min, Right_Line = Right_Max;

    for (j = Left_Min; j <= Right_Max; j++) //
    {

        if (data[i * 160 + j] < image_threshold) //检测黑点
        {
            Black_Leftline = j - 1;
            break; // 左右边界都找到，结束循环
        }
    }
    return (Black_Leftline);
}

/*
从右侧搜索黑边界
*/
unsigned char
Traversal_Black_Right(unsigned char i, unsigned char *data, unsigned char Left_Min, unsigned char Right_Max) {
    unsigned char j;
    unsigned char Black_Rightline;
//    unsigned char Left_Line = Left_Min, Right_Line = Right_Max;

    for (j = Right_Max; j >= Left_Min; j--) //
    {

        if (data[i * 160 + j] < image_threshold) //检测黑点
        {
            Black_Rightline = j + 1;
            break; // 左右边界都找到，结束循环
        }
    }
    return (Black_Rightline);
}

/**************************************************************************
   *从左侧开始搜索边界，返回赛道宽度
  *
  *   说明：本函数仅仅作为探测赛道使用，仅返回赛道宽度，不保存边界数据*/
unsigned char Traversal_Left(unsigned char i, unsigned char *data, unsigned char *Mid, unsigned char Left_Min,
                             unsigned char Right_Max) {
    unsigned char j, White_Flag = 0;
    unsigned char Left_Line = Left_Min, Right_Line = Right_Max;

    for (j = Left_Min; j <= Right_Max; j++) // 边界坐标 1到159
    {
        if (!White_Flag) // 先查找左边界
        {
            if (data[i * 160 + j] >= image_threshold) // 检测到白点
            {
                Left_Line = j;  // 记录当前j值为本行左边界
                White_Flag = 1; // 左边界已找到，必有右边界

                continue; // 结束本次循环，进入下次循环
            }
        } else {
            if (data[i * 160 + j] < image_threshold) //检测黑点
            {
                Right_Line = j - 1; //记录当前j值为本行右边界
                break;              // 左右边界都找到，结束循环
            }
        }
    }

    if (!White_Flag) // 未找到左右边界
    {
        return 0;
    } else {
        *Mid = (Right_Line + Left_Line) / 2;

        return (Right_Line - Left_Line);
    }
}

/**************************************************************************
   *从右侧开始搜索边界，返回赛道宽度
  *
  *   说明：本函数仅仅作为探测赛道使用，仅返回赛道宽度，不保存边界数据*/
unsigned char Traversal_Right(unsigned char i, unsigned char *data, unsigned char *Mid, unsigned char Left_Min,
                              unsigned char Right_Max) {
    unsigned char j, White_Flag = 0;
    unsigned char Left_Line = Left_Min, Right_Line = Right_Max;

    for (j = Right_Max; j >= Left_Min; j--) // 边界坐标 1到159
    {
        if (!White_Flag) // 先查找右边界
        {
            if (data[i * 160 + j] >= image_threshold) // 检测到白点
            {
                Right_Line = j; // 记录当前j值为本行右边界
                White_Flag = 1; // 右边界已找到，必有左边界

                continue; // 结束本次循环，进入下次循环
            }
        } else {
            if (data[i * 160 + j] < image_threshold) //检测黑点
            {
                Left_Line = j + 1; //记录当前j值为本行左边界
                break;             // 左右边界都找到，结束循环
            }
        }
    }

    if (!White_Flag) // 未找到左右边界
    {
        return 0;
    } else {
        *Mid = (Right_Line + Left_Line) / 2;

        return (Right_Line - Left_Line);
    }
}

/*
  *   从左侧开始搜索边界，保存赛道边界，返回1成功 0失败
  *
  *   说明：本函数使用后将保存边界数据
  */
unsigned char
Traversal_Left_Line(unsigned char i, unsigned char *data, unsigned char *Left_Line, unsigned char *Right_Line) {
    unsigned char j, White_Flag = 0;

    Left_Line[i] = 1;
    Right_Line[i] = 159;

    for (j = 1; j < 160; j++) // 边界坐标 1到159
    {
        if (!White_Flag) // 先查找左边界
        {
            if (data[i * 160 + j] >= image_threshold) // 检测到白点
            {
                Left_Line[i] = j; // 记录当前j值为本行左边界
                White_Flag = 1;   // 左边界已找到，必有右边界

                continue; // 结束本次循环，进入下次循环
            }
        } else {
            if (data[i * 160 + j] < image_threshold) //检测黑点
            {
                Right_Line[i] = j - 1; //记录当前j值为本行右边界

                break; // 左右边界都找到，结束循环
            }
        }
    }
    if (White_Flag) {
        Left_Add_Line[i] = Left_Line[i];
        Right_Add_Line[i] = Right_Line[i];
        Width_Real[i] = Right_Line[i] - Left_Line[i];
        Width_Add[i] = Width_Real[i];
    }

    return White_Flag; // 返回搜索结果
}

/*
  *   从右侧开始搜索边界，保存赛道边界，返回1成功 0失败
  *
  *   说明：本函数使用后将保存边界数据
  */
unsigned char
Traversal_Right_Line(unsigned char i, unsigned char *data, unsigned char *Left_Line, unsigned char *Right_Line) {
    unsigned char j, White_Flag = 0;

    Left_Line[i] = 1;
    Right_Line[i] = 159;

    for (j = 159; j > 0; j--) // 边界坐标 1到159
    {
        if (!White_Flag) // 先查找右边界
        {
            if (data[i * 160 + j] >= image_threshold) // 检测到白点
            {
                Right_Line[i] = j; // 记录当前j值为本行右边界
                White_Flag = 1;    // 右边界已找到，必有左边界

                continue; // 结束本次循环，进入下次循环
            }
        } else {
            if (data[i * 160 + j] < image_threshold) //检测黑点
            {
                Left_Line[i] = j + 1; //记录当前j值为本行左边界

                break; // 左右边界都找到，结束循环
            }
        }
    }
    if (White_Flag) {
        Left_Add_Line[i] = Left_Line[i];
        Right_Add_Line[i] = Right_Line[i];
        Width_Real[i] = Right_Line[i] - Left_Line[i];
        Width_Add[i] = Width_Real[i];
    }

    return White_Flag; // 返回搜索结果
}

/*
  *   从中间向两边搜索边界
  *
  *   说明：本函数使用后将保存边界数据
  */
void Traversal_Mid_Line(unsigned char i, unsigned char *data, unsigned char Mid, unsigned char Left_Min,
                        unsigned char Right_Max, unsigned char *Left_Line, unsigned char *Right_Line,
                        unsigned char *Left_Add_Line, unsigned char *Right_Add_Line) {
    unsigned char j;

    Left_Add_Flag[i] = 1; // 初始化补线标志位
    Right_Add_Flag[i] = 1;

    Left_Min = (unsigned char) func_limit_ab(Left_Min, 1, 159); // 限幅，防止出错
    if (Left_Add_Flag[i + 2]) {
        Left_Min = (unsigned char) func_limit_ab(Left_Min, Left_Add_Line[i + 2] - 30, 159);
    }
    Right_Max = (unsigned char) func_limit_ab(Right_Max, 1, 159);
    if (Right_Add_Flag[i + 2]) {
        Right_Max = (unsigned char) func_limit_ab(Right_Max, 1, Right_Add_Line[i + 2] + 30);
    }

    Right_Line[i] = Right_Max;
    Left_Line[i] = Left_Min;          //边界初始值
    for (j = Mid; j >= Left_Min; j--) // 以前一行中点为起点向左查找边界
    {
        if (data[i * 160 + j] < image_threshold) // 检测到黑点
        {
            Left_Add_Flag[i] = 0; //左边界不需要补线，清除标志位
            if (Start_caculate_Flag == 0) {
                Start_caculateline = i;
                Start_caculate_Flag = 1;
            }
            Left_Line[i] = j + 1;     //记录当前j值为本行实际左边界
            Left_Add_Line[i] = j + 1; // 记录实际左边界为补线左边界
            break;
        }
    }
    for (j = Mid; j <= Right_Max; j++) // 以前一行中点为起点向右查找右边界
    {
        if (data[i * 160 + j] < image_threshold) //检测到黑点
        {
            Right_Add_Flag[i] = 0; //右边界不需要补线，清除标志位
            if (Start_caculate_Flag == 0) {
                Start_caculateline = i;
                Start_caculate_Flag = 1;
            }
            Right_Line[i] = j - 1;     //记录当前j值为本行右边界
            Right_Add_Line[i] = j - 1; // 记录实际右边界为补线左边界
            break;
        }
    }
    if (Left_Add_Flag[i + 2]) // 左边界前一行需要补线
    {
        if (Left_Add_Line[i] <= Left_Add_Line[i + 2]) // 本行限定就要严格
        {
            Left_Add_Flag[i] = 1;
        }
    }
    if (Right_Add_Flag[i + 2]) // 右边界前一行需要补线
    {
        if (Right_Add_Line[i] >= Right_Add_Line[i + 2]) // 本行限定就要严格
        {
            Right_Add_Flag[i] = 1;
        }
    }
    if (Left_Add_Flag[i]) // 左边界需要补线
    {
        if (data[(i - 2) * 160 + Left_Add_Line[i + 2]] < image_threshold ||
            data[(i - 4) * 160 + Left_Add_Line[i + 2]] < image_threshold) // 可能是反光干扰
        {
            Left_Add_Flag[i] = 0;                    //左边界不需要补线，清除标志位
            Left_Line[i] = Left_Add_Line[i + 2];     //记录当前j值为本行实际左边界
            Left_Add_Line[i] = Left_Add_Line[i + 2]; // 记录实际左边界为补线左边界
        } else {
            if (i >= CountLine - 4) // 前6行
            {
                Left_Add_Line[i] = Left_Line[CountLine]; // 使用底行数据
            } else {
                Left_Add_Line[i] = Left_Add_Line[i + 2]; // 使用前2行左边界作为本行左边界
            }
        }
    }
    if (Right_Add_Flag[i]) // 右边界需要补线
    {
        if (data[(i - 2) * 160 + Right_Add_Line[i + 2]] < image_threshold ||
            data[(i - 4) * 160 + Right_Add_Line[i + 2]] < image_threshold) // 可能是反光干扰
        {
            Right_Add_Flag[i] = 0;                     //左边界不需要补线，清除标志位
            Right_Line[i] = Right_Add_Line[i + 2];     //记录当前j值为本行实际左边界
            Right_Add_Line[i] = Right_Add_Line[i + 2]; // 记录实际左边界为补线左边界
        } else {
            if (i >= CountLine - 4) // 前6行
            {
                Right_Add_Line[i] = Right_Line[CountLine]; // 使用底行数据
            } else {
                Right_Add_Line[i] = Right_Add_Line[i + 2]; // 使用前2行右边界作为本行右边界
            }
        }
    }
    Width_Real[i] = Right_Line[i] - Left_Line[i];        // 计算实际赛道宽度
    Width_Add[i] = Right_Add_Line[i] - Left_Add_Line[i]; // 计算补线赛道宽度
}

/*
  *   计算补线坐标
  *
  *   说明：使用两点法计算拟合出的补线坐标
  */
unsigned char Calculate_Add(unsigned char i, float Ka, float Kb) // 计算补线坐标
{
    float res;
    sint32 Result;

    res = i * Ka + Kb;
    Result = (sint32) func_limit_ab(res, 1, 159);

    return (unsigned char) Result;
}

/*
  *   两点法求直线
  *
  *   说明：拟合直线 y = Ka * x + Kb   Mode == 1代表左边界，Mode == 2代表右边界
  */
void Curve_Fitting(float *Ka, float *Kb, unsigned char *Start, unsigned char *Line, unsigned char *Add_Flag,
                   unsigned char Mode) {
    *Start += 4;
    if (Add_Flag[*Start] == 2) {
        if (*Start <= CountLine - 8) {
            *Start += 2;
        }
        *Ka = 1.0 * (Line[*Start + 4] - Line[*Start]) / 4;
        if (Mode == 2) {
            if (*Ka < 0) {
                *Ka = 0;
            }
        }
        if (Mode == 1) {
            if (*Ka > 0) {
                *Ka = 0;
            }
        }
    } else {
        *Ka = 1.0 * (Line[*Start + 4] - Line[*Start]) / 4;
    }
    *Kb = 1.0 * Line[*Start] - (*Ka * (*Start));
}

/*
  *   补线修复
  *
  *   说明：有始有终才使用，直接使用两点斜率进行补线
  */

void
Line_Repair(unsigned char Start, unsigned char Stop, unsigned char *data, unsigned char *Line, unsigned char *Line_Add,
            unsigned char *Add_Flag, unsigned char Mode) {
    float res;
    unsigned char i, End; // 控制行
    float Ka, Kb;

    if ((Mode == 1) && (Right_Add_Start <= Stop) && Stop && Start <= CountLine - 6) // 左边界补线
    {
        for (i = Start + 2; i >= Stop + 2;) {
            i -= 2;
            Line_Add[i] = (unsigned char) func_limit_ab(Right_Add_Line[i] - Width_Add[i + 2] + 5, 1, Right_Add_Line[i]);
            Width_Add[i] = Width_Add[i + 2] - 3;

            if (Width_Add[i] <= 10) {
                Line_Count = i;
                break;
            }
        }
    }
    if ((Mode == 2) && (Left_Add_Start <= Stop) && Stop && Start <= CountLine - 6) // 右边界补线
    {
        for (i = Start + 2; i >= Stop + 2;) {
            i -= 2;
            Line_Add[i] = (unsigned char) func_limit_ab(Left_Add_Line[i] + Width_Add[i + 2] - 5, Left_Add_Line[i], 159);
            Width_Add[i] = Width_Add[i + 2] - 3;

            if (Width_Add[i] <= 10) {
                Line_Count = i;
                break;
            }
        }
    } else {
        if (Stop) // 有始有终
        {
            if (((Mode == 1 && Left_Add_Start >= CountLine - 4) || (Mode == 2 && Right_Add_Start >= CountLine - 4)) &&
                ((Mode == 1 && Line_Add[Left_Add_Stop] > Line_Add[CountLine]) ||
                 (Mode == 2 && Line_Add[Right_Add_Stop] < Line_Add[CountLine]))) // 只有较少行需要补线
            {
                if (Stop >= Line_Count + 4) {
                    End = Stop - 4;
                } else if (Stop >= Line_Count + 2) {
                    End = Stop - 2;
                } else {
                    End = 0;
                }
                if (End) {
                    Ka = 1.0 * (Line_Add[Stop] - Line_Add[End]) / (Stop - End);
                    Kb = 1.0 * Line_Add[Stop] - (Ka * Stop);

                    if (Mode == 1) {
                        if (Line_Add[CountLine] > CountLine * Ka + Kb) {
                            Ka = 1.0 * (Line_Add[Start] - Line_Add[Stop - 2]) / (Start - (Stop - 2));
                            Kb = 1.0 * Line_Add[Start] - (Ka * Start);
                        }
                    } else if (Mode == 2) {
                        if (Line_Add[CountLine] < CountLine * Ka + Kb) {
                            Ka = 1.0 * (Line_Add[Start] - Line_Add[Stop - 2]) / (Start - (Stop - 2));
                            Kb = 1.0 * Line_Add[Start] - (Ka * Start);
                        }
                    }
                } else {
                    Ka = 1.0 * (Line_Add[Start] - Line_Add[Stop]) / (Start - Stop);
                    Kb = 1.0 * Line_Add[Start] - (Ka * Start);
                }

                for (i = CountLine + 2; i > Stop;) {
                    i -= 2;
                    res = i * Ka + Kb;
                    Line_Add[i] = (unsigned char) func_limit_ab(res, 1, 159);
                }
            } else // 将起始行和结束行计算斜率补线
            {
                if (Start <= CountLine - 2) {
                    Start += 2;
                }
                if (Stop >= Line_Count + 4) {
                    Stop -= 4;
                } else if (Stop >= Line_Count + 2) {
                    Stop -= 2;
                }
                Ka = 1.0 * (Line_Add[Start] - Line_Add[Stop]) / (Start - Stop);
                Kb = 1.0 * Line_Add[Start] - (Ka * Start);

                if (Mode == 1) {
                    if (Line_Add[CountLine] > CountLine * Ka + Kb) {
                        Ka = 1.0 * (Line_Add[Start] - Line_Add[Stop]) / (Start - Stop);
                        Kb = 1.0 * Line_Add[Start] - (Ka * Start);
                    }
                } else if (Mode == 2) {
                    if (Line_Add[CountLine] < CountLine * Ka + Kb) {
                        Ka = 1.0 * (Line_Add[Start] - Line_Add[Stop]) / (Start - Stop);
                        Kb = 1.0 * Line_Add[Start] - (Ka * Start);
                    }
                }

                for (i = Stop; i <= Start;) {
                    i += 2;
                    res = i * Ka + Kb;
                    Line_Add[i] = (unsigned char) func_limit_ab(res, 1, 159);
                }
            }
        }
    }
}

/*
  *   中线修复
  *
  *   说明：普通弯道丢线使用平移赛道方式，中点到达边界结束
  */
void Mid_Line_Repair(unsigned char count, unsigned char *data) {

    unsigned char i; // 控制行
    for (i = CountLine + 2; i >= count + 2; i -= 2) {
        Mid_Line[i] = (Left_Add_Line[i] >> 1) + (Right_Add_Line[i] >> 1); // 计算赛道中点
        Width_Add[i] = Right_Add_Line[i] - Left_Add_Line[i]; // 计算赛道宽度
    }
    Mid_Line[CountLine + 2] = Mid_Line[CountLine];
}


char Error_Transform(unsigned char Data, unsigned char Set) {
    char Error;

    Error = Set - Data;
    if (Error < 0) {
        Error = -Error;
    }

    return Error;
}


int16 Point_Average(void) {
//5.16
    int8 weight[][4] = {{1, 3, 6, 2},
                        {1, 2, 4, 1},
                        {1, 2, 3, 1}};
//    int8  weight[][4]={{2,5,3,1},
//                       {1,2,4,1},
//                       {1,3,4,1}};
//    int8  weight[][4]={{1,3,5,2},
//                       {1,2,4,1},
//                       {1,3,4,1}};
    int8 number1 = 0, number2 = 0, number3 = 0, number4 = 0;
    unsigned char i, Point;
    int32 Sum = 0;
    static unsigned char Last_Point = 80;
    char value = 0;
    unsigned char point_mid;


    if (Left_Ring_flag || Right_Ring_flag)
        value = 1;
    if (Three_cross_count % 2 == 1) {
        value = 2;
        //Line_Count=CountLine-Straight_Counter;
    }

    for (i = CountLine; i >= Line_Count + 2;) {
        i -= 2;
        if (i >= 80) {
            Sum += Mid_Line[i] * weight[value][0];
            number1++;
        } else if (i >= 60) {
            Sum += Mid_Line[i] * weight[value][1];
            number2++;
        } else if (i >= 30) {

            Sum += Mid_Line[i] * weight[value][2];
            number3++;
        } else {
            Sum += Mid_Line[i] * weight[value][3];
            number4++;
        }

    }

//limit
    Point = Sum / (weight[value][0] * number1 + weight[value][1] * number2 + weight[value][2] * number3 +
                   weight[value][3] * number4);  // 对中线求平均
    //    Point = Sum / ((CountLine-Line_Count)/2);  // 对中线求平均
    Last_Point = Point;
    Point = Point * 0.8 + Last_Point * 0.2; // 低通滤波
    Point = func_limit_ab(Point, 1, 159);

    //前瞻计算
    if (Line_Count >= 36) {
        point_mid = Mid_Line[36];
    } else {
        point_mid = Mid_Line[CountLine - 36];
    }

    foresight = 0.8 * Error_Transform(point_mid, 80) + 0.2 * Error_Transform(Point, 80);

    return (Point - 80);


}


void Draw_Cross(void) {
    unsigned char j, i;
    for (j = 0; j <= 69; j += 2) {
        ips200_draw_point(80, j, RGB565_YELLOW);
    }
    for (i = 0; i <= 180; i += 2) {
        ips200_draw_point(i, 34, RGB565_YELLOW);  //画十字
    }
}

void show_add_line(void) {
    //Draw_Cross();    //画十字
    for (int i = CountLine + 2; i >= Line_Count + 2; i -= 2) {
        ips200_draw_point((uint16)(Mid_Line[i]          ), (uint16) i, RGB565_RED );
        ips200_draw_point((uint16)(Mid_Line[i] + 1      ), (uint16) i, RGB565_RED );//将中线显示在屏上
        ips200_draw_point((uint16)(Mid_Line[i] - 1      ), (uint16) i, RGB565_RED );
        ips200_draw_point((uint16)(Left_Add_Line[i]     ), (uint16) i, RGB565_BLUE);
        ips200_draw_point((uint16)(Left_Add_Line[i] + 1 ), (uint16) i, RGB565_BLUE);        //显示左右边界
        ips200_draw_point((uint16)(Right_Add_Line[i]    ), (uint16) i, RGB565_BLUE);
        ips200_draw_point((uint16)(Right_Add_Line[i] - 1), (uint16) i, RGB565_BLUE);

    }
}

void show_line(void) {
    for (int i = CountLine + 2; i >= Line_Count + 2; i -= 2) {
        ips200_draw_point((uint16)(Mid_Line[i]      ),(uint16) i, RGB565_RED );
        ips200_draw_point((uint16)(Mid_Line[i] + 1  ),(uint16) i, RGB565_RED );//将中线显示在屏上
        ips200_draw_point((uint16)(Mid_Line[i] - 1  ),(uint16) i, RGB565_RED );
        ips200_draw_point((uint16)(Left_Line[i]     ),(uint16) i, RGB565_BLUE);
        ips200_draw_point((uint16)(Left_Line[i] + 1 ),(uint16) i, RGB565_BLUE);        //显示左右边界
        ips200_draw_point((uint16)(Right_Line[i]    ),(uint16) i, RGB565_BLUE);
        ips200_draw_point((uint16)(Right_Line[i] - 1),(uint16) i, RGB565_BLUE);
    }
}

void show_image_information() {
    register short i = 70;
    ips200_show_string(0, i, "difference");
    ips200_show_int(100, i, difference, 3);
    i++;
    ips200_show_string(0, i, "servo_duty");
    ips200_show_int(100, i, servo_duty, 3);
    i++;
    ips200_show_string(0, i, "first_line");
    ips200_show_uint(100, i, Right_Line[CountLine] - Left_Line[CountLine], 3);
    i++;
    ips200_show_string(0, i, "line_count");
    ips200_show_uint(110, i, Line_Count, 3);
    i++;

    ips200_show_string(0, i, "Left_start");
    ips200_show_uint(140, i, Left_Add_Start, 3);
    ips200_show_uint(170, i, Left_Add_Line[Left_Add_Start], 3);
    i++;
    ips200_show_string(0, i, "Right_Start");
    ips200_show_uint(140, i, Right_Add_Start, 3);
    ips200_show_uint(170, i, Right_Add_Line[Right_Add_Start], 3);
    i++;
    ips200_show_string(0, i, "Left_Stop");
    ips200_show_uint(140, i, Left_Add_Stop, 3);
    ips200_show_uint(170, i, Left_Add_Line[Left_Add_Stop], 3);
    i++;
    ips200_show_string(0, i, "Right_Stop");
    ips200_show_uint(140, i, Right_Add_Stop, 3);
    ips200_show_uint(170, i, Right_Add_Line[Right_Add_Stop], 3);
    i++;

    ips200_show_string(0, i, "Straight_Counter");
    ips200_show_uint(140, i, Straight_Counter, 3);
    i++;
    ips200_show_string(0, i, "MIN_HIGH");
    ips200_show_uint(110, i, Min_high, 3);
    i++;
    ips200_show_string(0, i, "MAX_HIGH");
    ips200_show_uint(110, i, Max_high, 3);
    i++;

    ips200_show_string(0, i, "left_turn");
    ips200_show_uint(110, i, left_turn, 3);
    i++;
    ips200_show_string(0, i, "right_turn");
    ips200_show_uint(110, i, right_turn, 3);
    i++;

    //ips200_show_string (0,i,"foresight"); ips200_show_uint8(110,i,foresight);i++;

}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     图像处理封装函数
// 参数说明
// 返回参数
// 使用示例     Image_Process();
// 备注信息     主要使用在主函数当中
// 作者信息     Created by 57Kindness on 2023/5/25.
//-------------------------------------------------------------------------------------------------------------------
void Image_Process(void) {
    if (mt9v03x_finish_flag) {
        Get_Use_Image();
        image_threshold = GetOSTU(Image_Use);
        Get_Bin_Images(image_threshold);
        Image_Handle(Image_Use[0]);
        Last_Line_Count = Line_Count;
        //计算偏差
        difference = Point_Average();
//        if (difference >=  25) difference =  25;
//        if (difference <= -25) difference = -25;
        Turn_Difference = difference;
        mt9v03x_finish_flag = 0;
    }
}
