# **_参数表_**

### 5_1参数
````
PP                  50.500 
PI                  0
PD                  8.0
AP                  -17
AI                  0
AD                  -12
SP                  0.7
SI                  0.07
SD                  0.009
````
<span style="color: #006600; "> Created by 57Kindness on 2023/5/1.</span> <br/>

### 5_6参数
````
PP                  40
PI                  0
PD                  20.0
AP                  -23
AI                  0
AD                  -96
SP                  0
SI                  0.06
SD                  0
````
<span style="color: #006600; "> Created by 57Kindness on 2023/5/7.</span> <br/>

### 5_25_2_23参数
````
PP                  40
PI                  0.939
PD                  20.0
AP                  -24
AI                  -0.029
AD                  -100
SP                  0
SI                  -0.01
SD                 -0.09
````
<span style="color: #006600; "> Created by 57Kindness on 2023/5/7.</span> <br/>

### 5_25_2_23参数
````
PP                  40
PI                  0.939
PD                  20.0
AP                  -24
AI                  -0.029
AD                  -100
SP                  0
SI                  -0.01
SD                 -0.09
AZ                  -2.119
AZ2                 -2.119
````
<span style="color: #006600; "> Created by 57Kindness on 2023/5/25.</span> <br/>

### 5_25_9_37参数
````
PP                  40
PI                  0.939
PD                  20.0
AP                  -24
AI                  -0.029
AD                  -100
SP                  0
SI                  -0.01
SD                  -0.09
BP                  -700
BI                  0
BD                  -1.24
VP                  0.029
VI                 -16
VD                  0.119
AZ                  -2.219
AZ2                 0.1
````
<span style="color: #006600; "> Created by 57Kindness on 2023/5/25.</span> <br/>

